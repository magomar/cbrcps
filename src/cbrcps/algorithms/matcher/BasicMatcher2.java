/**
 *
 */
package cbrcps.algorithms.matcher;

import cbrcps.algorithms.data.JobComparator;
import cbrcps.algorithms.data.ProjectMatching;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;
import java.util.*;

/**
 * @author Mario Gomez
 */
public final class BasicMatcher2 extends AbstractProjectMatcher {

    /* (non-Javadoc)
     * @see cbrcps.algorithms.matcher.ProjectMatcher#matchProjects(rcpsp.data.jaxb.Project, rcpsp.data.jaxb.Project)
     */
    @Override
    public synchronized ProjectMatching matchProjects(Project source, Project target) {
        double similarity = 0;
        Map<Job, Job> map = new HashMap<Job, Job>();
        if (matchResources(source, target)) {
            // Compute job mappings
            map = mapJobs(source, target);
            similarity = (double) map.keySet().size() / target.getNumJobs();
        }

        return new ProjectMatching(similarity, toIntegerMap(map));
    }

    /**
     * @param sourceJob
     * @param targetJob
     * @return
     */
    static protected boolean matchMode(Job sourceJob, Job targetJob) {
        boolean match = true;
        Mode sourceMode = sourceJob.getMode().get(0);
        Mode targetMode = targetJob.getMode().get(0);
        if (sourceMode.getDuration() != targetMode.getDuration()) {
            match = false;
        } else if (sourceMode.getResourceRequest().size() != targetMode.getResourceRequest().size()) {
            match = false;
        } else {
            List<Integer> sourceResourceRequests = sourceMode.getResourceRequest();
            List<Integer> targetResourceRequests = targetMode.getResourceRequest();

            for (int k = 0; k < sourceResourceRequests.size(); k++) {
                if (sourceResourceRequests.get(k) != targetResourceRequests.get(k)) {
                    match = false;
                    break;
                }
            }
        }
        return match;
    }

    static protected boolean matchSuccessors(Job sourceJob, Job targetJob, Project source, Project target) {
        boolean match = true;
//		Integer lastSourceJob = source.getNumJobs() - 1;
//		Integer lastTargetJob = target.getNumJobs() - 1;
//		if (!sourceJob.getSuccessors().contains(lastSourceJob) && !targetJob.getSuccessors().contains(lastTargetJob)) {
//			Map<Job,Job> successorsMap = mapSuccessors(sourceJob, targetJob, source, target);
//			if (successorsMap.keySet().size() != sourceJob.getSuccessors().size()) {
//				match = false;
//			}
//		}
        return match;
    }

    static protected Map<Job, Job> mapSuccessors(Job sourceJob, Job targetJob, Project source, Project target) {
        List<Job> sourceSuccessors = toJobs(sourceJob.getSuccessor(), source);
        List<Job> targetSuccessors = toJobs(targetJob.getSuccessor(), target);
        Map<Job, List<Job>> potentialMappings = new TreeMap<Job, List<Job>>(new JobComparator());
        Map<Job, Job> successorMap = new HashMap<Job, Job>();
        // Compare jobs, select as potential mappings those having same duration and resource requests
        for (Job sourceSuccessor : sourceSuccessors) {
            List<Job> jobMatches = new ArrayList<Job>();
            for (Job targetSuccessor : targetSuccessors) {
                if (matchMode(sourceSuccessor, targetSuccessor)) {
                    jobMatches.add(targetSuccessor);
                }
            }
            potentialMappings.put(sourceSuccessor, jobMatches);
        }

        // For every successor of the source select a single mapping  
        List<Job> mappedJobs = new ArrayList<Job>();
        for (Job sourceSuccessor : potentialMappings.keySet()) {
            for (Job targetSuccessor : potentialMappings.get(sourceSuccessor)) {
                if (!mappedJobs.contains(targetSuccessor)) {
                    successorMap.put(sourceSuccessor, targetSuccessor);
                    mappedJobs.add(targetSuccessor);
                    break;
                }
            }
        }
        return successorMap;
    }

    static protected Map<Job, Job> mapJobs(Project source, Project target) {
        Map<Job, List<Job>> potentialMappings = new TreeMap<Job, List<Job>>(new JobComparator());
        Map<Job, Job> jobMap = new HashMap<Job, Job>();
        // Compare jobs, select as potential mappings those having same duration and resource requests
        for (Job sourceJob : source.getJob()) {
            List<Job> jobMatches = new ArrayList<Job>();
            for (Job targetJob : target.getJob()) {
                if (matchMode(sourceJob, targetJob) && matchSuccessors(sourceJob, targetJob, source, target)) {
                    jobMatches.add(targetJob);
                }
            }
            potentialMappings.put(sourceJob, jobMatches);
        }

        // For every job in the list of source jobs select only one job among the potential mappings  
        List<Job> mappedJobs = new ArrayList<Job>();
        for (Job sourceJob : potentialMappings.keySet()) {
            for (Job targetJob : potentialMappings.get(sourceJob)) {
                if (!mappedJobs.contains(targetJob)) {
                    jobMap.put(sourceJob, targetJob);
                    mappedJobs.add(targetJob);
                    break;
                }
            }
        }
        return jobMap;
    }

    /**
     * @param source
     * @param target
     * @return
     */
    static protected boolean matchResources(Project source, Project target) {
        boolean match = true;
        if (source.getNumRenewableResources() != target.getNumRenewableResources()
                || source.getNumNonRenewableResources() != target.getNumNonRenewableResources()) {
            match = false;
        } else {
            List<Resource> sourceResources = source.getResource();
            List<Resource> targetResources = target.getResource();
            for (int k = 0; k < source.getNumRenewableResources() + source.getNumNonRenewableResources(); k++) {
                if (sourceResources.get(k).getCapacity() != targetResources.get(k).getCapacity()) {
                    match = false;
                }
            }
        }
        return match;
    }

    static private List<Job> toJobs(List<Integer> integerList, Project project) {
        List<Job> jobs = new ArrayList<Job>();
        for (Integer j : integerList) {
            jobs.add(project.getJob().get(j));
        }
        return jobs;
    }

    static private Map<Integer, Integer> toIntegerMap(Map<Job, Job> jobMap) {
        Map<Integer, Integer> integerMap = new HashMap<Integer, Integer>();
        for (Job sourceJob : jobMap.keySet()) {
            Job targetJob = jobMap.get(sourceJob);
            integerMap.put(sourceJob.getId(), targetJob.getId());
        }
        return integerMap;
    }
}
