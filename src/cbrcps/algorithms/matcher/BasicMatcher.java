/**
 *
 */
package cbrcps.algorithms.matcher;

import cbrcps.algorithms.data.JobComparator;
import cbrcps.algorithms.data.ProjectMatching;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;
import java.util.*;

/**
 * @author Mario Gomez
 */
public final class BasicMatcher extends AbstractProjectMatcher {


    /* (non-Javadoc)
     * @see cbrcps.algorithms.matcher.ProjectMatcher#match(rcpsp.data.jaxb.Project, rcpsp.data.jaxb.Project)
     */
    @Override
    public synchronized ProjectMatching matchProjects(Project source, Project target) {
        double similarity = 0;
        Map<Job, Job> map = new HashMap<Job, Job>();
        if (matchResources(source, target)) {
            // Compute job mappings
            map = mapJobs(source.getJob(), target.getJob());
            similarity = (double) map.keySet().size() / target.getNumJobs();
        }

        return new ProjectMatching(similarity, toIntegerMap(map));
    }

    /**
     * @param sourceJob
     * @param targetJob
     * @return
     */
    static protected boolean matchMode(Job sourceJob, Job targetJob) {
        boolean match = true;
        Mode sourceMode = sourceJob.getMode().get(0);
        Mode targetMode = targetJob.getMode().get(0);
        if (sourceMode.getDuration() != targetMode.getDuration()) {
            match = false;
        } else if (sourceMode.getResourceRequest().size() != targetMode.getResourceRequest().size()) {
            match = false;
        } else {
            List<Integer> sourceResourceRequests = sourceMode.getResourceRequest();
            List<Integer> targetResourceRequests = targetMode.getResourceRequest();

            for (int k = 0; k < sourceResourceRequests.size(); k++) {
                if (sourceResourceRequests.get(k) != targetResourceRequests.get(k)) {
                    match = false;
                    break;
                }
            }
        }
        return match;
    }

    static protected boolean matchSuccessors(Job sourceJob, Job targetJob, Project source, Project target) {
        boolean match = true;
        Map<Job, Job> successorsMap = mapJobs(toJobs(sourceJob.getSuccessor(), source), toJobs(targetJob.getSuccessor(), target));
        if (successorsMap.keySet().size() != sourceJob.getSuccessor().size()) {
            match = false;
        }
        return match;
    }

    static protected Map<Job, Job> mapJobs(List<Job> sourceJobs, List<Job> targetJobs) {
        Map<Job, List<Job>> potentialMatches = new TreeMap<>(new JobComparator());
        Map<Job, Job> jobMap = new HashMap<>();
        // Compare jobs, select as potential mappings those having same duration and resource requests
        for (Job sourceJob : sourceJobs) {
            List<Job> jobMatches = new ArrayList<>();
            for (Job targetJob : targetJobs) {
                if (matchMode(sourceJob, targetJob)) {
                    jobMatches.add(targetJob);
                }
            }
            potentialMatches.put(sourceJob, jobMatches);
        }

        // For every job in the list of source jobs select only one mapping among the potential mappings  
        List<Job> mappedJobs = new ArrayList<>();
        for (Job sourceJob : potentialMatches.keySet()) {
            for (Job targetJob : potentialMatches.get(sourceJob)) {
                if (!mappedJobs.contains(targetJob)) {
                    jobMap.put(sourceJob, targetJob);
                    mappedJobs.add(targetJob);
                    break;
                }
            }
        }
        return jobMap;
    }

    /**
     * @param source
     * @param target
     * @return
     */
    static protected boolean matchResources(Project source, Project target) {
        boolean match = true;
        if (source.getNumRenewableResources() != target.getNumRenewableResources()
                || source.getNumNonRenewableResources() != target.getNumNonRenewableResources()
                ) {
            match = false;
        } else {
            List<Resource> sourceResources = source.getResource();
            List<Resource> targetResources = target.getResource();
            for (int k = 0; k < source.getNumRenewableResources()+source.getNumNonRenewableResources(); k++) {
                if (sourceResources.get(k).getCapacity() != targetResources.get(k).getCapacity()) {
                    match = false;
                }
            }
        }
        return match;
    }

    static private List<Job> toJobs(List<Integer> integerList, Project project) {
        List<Job> jobs = new ArrayList<>();
        for (Integer j : integerList) {
            jobs.add(project.getJob().get(j));
        }
        return jobs;
    }

    static private Map<Integer, Integer> toIntegerMap(Map<Job, Job> jobMap) {
        Map<Integer, Integer> integerMap = new HashMap<>();
        for (Job sourceJob : jobMap.keySet()) {
            Job targetJob = jobMap.get(sourceJob);
            integerMap.put(sourceJob.getId(), targetJob.getId());
        }
        return integerMap;
    }
}
