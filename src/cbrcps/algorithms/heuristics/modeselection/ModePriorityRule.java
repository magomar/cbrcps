/**
 * 
 */
package cbrcps.algorithms.heuristics.modeselection;

import cbrcps.algorithms.heuristics.PriorityRule;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;
import java.util.List;

/**
 * @author mario
 *
 */
public interface ModePriorityRule extends PriorityRule {

	/**
	 * @param modes
	 * @param resource
	 */
	public void setData(List<Mode>[] modes, Resource[] resource);
	/**
	 * @param selectedJob
	 * Important ! This method must be called before using the comparator (eg. Collections.sort(...))
	 */
	public void setSelectedJob(int selectedJob);
}
