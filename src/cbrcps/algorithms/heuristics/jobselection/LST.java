/**
 *
 */
package cbrcps.algorithms.heuristics.jobselection;

import java.util.ArrayList;

import cbrcps.algorithms.heuristics.PriorityRule;
import java.util.List;


/**
 * @author Mario Gomez
 *
 */
public final class LST extends AbstractJobPriorityRule {


	@Override
	public int compare(Integer o1, Integer o2) {
		int i1 = o1.intValue();
		int i2 = o2.intValue();
		return timeBounds[i1].getLatestStartTime() - timeBounds[i2].getLatestStartTime();
	}

	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			int j = jobList.get(i).intValue();
			priorityValue[i] = timeBounds[j].getLatestStartTime();
		}
		return priorityValue;
	}

}
