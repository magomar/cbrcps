/**
 *
 */
package cbrcps.algorithms.heuristics.jobselection;

import cbrcps.algorithms.data.TimeBounds;
import cbrcps.algorithms.heuristics.PriorityRule;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class MultiPriorityRule extends AbstractJobPriorityRule {
	final JobPriorityRule[] priorityRule;
	final double[] weight;

	/**
	 * @param priorityRule
	 * @param weight
	 */
	public MultiPriorityRule(JobPriorityRule[] priorityRule, double[] weight) {
		this.priorityRule = priorityRule;
		this.weight = weight;
	}


	@Override
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
		for (int i = 0; i < priorityRule.length; i++) {
			priorityRule[i].setData(timeBounds, successors, modes, resource);
		}
	}


	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[][] singlePriority = new double[priorityRule.length][jobList.size()];
		for (int i = 0; i < priorityRule.length; i++) {
			singlePriority[i] = priorityRule[i].getPriorityValues(jobList);
		}
		double[] compositePriority = new double[jobList.size()];
		for (int j = 0; j < jobList.size(); j++) {
			compositePriority[j] = 0;
			for (int i = 0; i < priorityRule.length; i++) {
				compositePriority[j] += weight[i] * singlePriority[i][j];
			}

		}
		return compositePriority;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Integer o1, Integer o2) {
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(o1);
		integerList.add(o2);
		double[] pv =getPriorityValues(integerList);
		double diff = pv[0] - pv[1];
//		return ( diff != 0 ? (int) diff : o1.intValue() - o2.intValue());
		return (int) diff;
	}

	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.jobselection.JobPriorityRule#toString()
	 */
	@Override
	public String toString() {
		List<String> ruleDescription = new ArrayList<String>();
		for (int i = 0; i < priorityRule.length; i++) {
			if (weight[i] == 1.0)
				ruleDescription.add(priorityRule[i].toString());
			else ruleDescription.add(weight[i] + priorityRule[i].toString());
		}
		return ruleDescription.toString();
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.PriorityRule#newInstance()
	 */
	@Override
	public PriorityRule newInstance() {
		JobPriorityRule[] newPriorityRule = new JobPriorityRule[priorityRule.length];
		for (int i = 0; i < priorityRule.length; i++) {
			newPriorityRule[i] = ((JobPriorityRule) priorityRule[i].newInstance());
		}
		return new MultiPriorityRule(newPriorityRule, weight);
	}
}
