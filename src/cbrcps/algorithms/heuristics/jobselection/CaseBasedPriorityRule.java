/**
 * 
 */
package cbrcps.algorithms.heuristics.jobselection;

import cbrcps.algorithms.matcher.ProjectMatcher;
import cbrcps.cbr.CBRCPS;
import rcpsp.data.jaxb.Project;
import cbrcps.wrappers.DatasetWrapper;

/**
 * @author Mario Gomez
 */
public interface CaseBasedPriorityRule {

	/**
	 * @param project
	 * @param jobPriorityRule
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public void build(Project project, JobPriorityRule jobPriorityRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher);
}
