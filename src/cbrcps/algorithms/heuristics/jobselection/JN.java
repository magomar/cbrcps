/**
 * 
 */
package cbrcps.algorithms.heuristics.jobselection;

import java.util.List;


/**
 * @author Mario Gomez
 *
 */
public final class JN extends AbstractJobPriorityRule {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Integer o1, Integer o2) {
		return o1 - o2;
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.JobComparatorInterface#getProbabilityValues()
	 */
	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			priorityValue[i] = jobList.get(i);
		}
		return priorityValue;
	}
}
