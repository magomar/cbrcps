/**
 * 
 */
package cbrcps.algorithms.heuristics.jobselection;

import cbrcps.algorithms.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class TRS extends AbstractJobPriorityRule {
	private List<Double>[] trs;
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.jobselection.JobPriorityRule#setData(cbrcps.algorithms.data.TimeBounds[], java.util.ArrayList<java.lang.Integer>[], java.util.ArrayList<rcpsp.data.jaxb.Mode>[])
	 */
	@Override
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
		super.setData(timeBounds, successors, modes, resource);
		trs = new ArrayList[modes.length]; // length = num jobs
		for (int j = 0; j < modes.length; j++) {
			trs[j] = new ArrayList<Double>();
			for (int m = 0;  m < modes[j].size(); m++) {
				double sumResourceUsage = 0;
				int resourceID = 0;
				for (Integer request : modes[j].get(m).getResourceRequest()) {
					double value = (double) request.intValue() / (double) resource[resourceID].getCapacity();
					sumResourceUsage += value;
					resourceID++;
				}
				trs[j].add(sumResourceUsage);
			}
		}
	}
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Integer o1, Integer o2) {
		int i1 = o1.intValue();
		int i2 = o2.intValue();
		return trs[i1].get(0).intValue() - trs[i2].get(0).intValue();
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.JobComparatorInterface#getProbabilityValues()
	 */
	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			int j = jobList.get(i).intValue();
			priorityValue[i] = trs[j].get(0).doubleValue();
		}
		return priorityValue;
	}

}
