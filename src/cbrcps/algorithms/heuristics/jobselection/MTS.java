/**
 *
 */
package cbrcps.algorithms.heuristics.jobselection;

import cbrcps.algorithms.data.TimeBounds;
import java.util.HashSet;
import java.util.List;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

/**
 * @author Mario Gomez
 *
 */
public final class MTS extends AbstractJobPriorityRule {

    private HashSet<Integer>[] extendedSuccessors;

    @Override
    public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
        super.setData(timeBounds, successors, modes, resource);
        // Pre-compute direct + indirect successors
        extendedSuccessors = new HashSet[successors.length];
        for (int j = 0; j < successors.length; j++) {
            extendedSuccessors[j] = new HashSet<>();
            addSuccessors(j, successors[j]);
        }
    }

    /**
     * @param job
     * @param jobList
     */
    private void addSuccessors(int job, List<Integer> jobList) {
        extendedSuccessors[job].addAll(jobList);
        for (Integer successor : jobList) {
            addSuccessors(job, successors[successor.intValue()]);
        }
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        return extendedSuccessors[i2].size() - extendedSuccessors[i1].size();
    }

    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i).intValue();
            priorityValue[i] = extendedSuccessors[jobList.get(0).intValue()].size() - extendedSuccessors[j].size();
        }
        return priorityValue;
    }
}
