/**
 * 
 */
package cbrcps.algorithms.heuristics.jobselection;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Mario Gomez
 *
 */
public final class EST extends AbstractJobPriorityRule {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Integer o1, Integer o2) {
		int i1 = o1.intValue();
		int i2 = o2.intValue();
		return timeBounds[i1].getEarliestStartTime() - timeBounds[i2].getEarliestStartTime();
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.heuristics.JobComparatorInterface#getProbabilityValues()
	 */
	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			int j = jobList.get(i);
			priorityValue[i] =timeBounds[j].getEarliestStartTime();
		}
		return priorityValue;
	}
}
