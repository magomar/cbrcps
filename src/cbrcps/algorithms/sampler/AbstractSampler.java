/**
 * 
 */
package cbrcps.algorithms.sampler;

import cbrcps.algorithms.heuristics.PriorityRule;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractSampler implements Sampler {
	private Random random = new Random(System.currentTimeMillis());

	public List<Integer> sampleOne(List<Integer> alternatives, PriorityRule priorityRule) {
		double[] prob = getProbabilityValues(alternatives, priorityRule);
		List<Integer> selection = new ArrayList<Integer>();
		double probAcum = 0;
		double randomEvent = random.nextDouble();
		for (int index = 0; index < prob.length; index++) {
			probAcum += prob[index];
			if (randomEvent <= probAcum) {
				selection.add(alternatives.get(index));
				break;
			}
		}
		return selection;
	}
	

	public List<Integer> sample(List<Integer> alternatives, PriorityRule priorityRule, int sampleSize) {
		double[] prob;
		List<Integer> selection = new ArrayList<Integer>();
		while (selection.size() < sampleSize && alternatives.size() > 0) {
			prob = getProbabilityValues(alternatives, priorityRule);
			double probAcum = 0;
			double randomEvent = random.nextDouble();
			for (int index = 0; index < prob.length; index++) {
				probAcum += prob[index];
				if (randomEvent <= probAcum) {
					selection.add(alternatives.get(index));
					alternatives.remove(index);
					break;
				}
			}
		}
//		selection.addAll(alternatives);
		return selection;
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.sampler.Sampler#newInstance()
	 */
	@Override
	public Sampler newInstance() {
		Sampler newInstance = null;
		try {
			newInstance  = this.getClass().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return newInstance;
	}
	
}
