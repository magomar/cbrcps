package cbrcps.algorithms.data;

import java.util.Comparator;

/**
 * @author Mario Gomez
 *
 */
public final class FinishTimeComparator implements Comparator<Integer> {
	private final int[] finishTime;

	/**
	 * @param finishTime
	 */
	public FinishTimeComparator(int[] finishTime) {
		this.finishTime = finishTime;
	}

	@Override
	public int compare(Integer o1, Integer o2) {
		return finishTime[o1.intValue()] - finishTime[o2.intValue()];
	}

}
