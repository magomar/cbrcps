package cbrcps.algorithms.data;

import java.io.Serializable;

/**
 * @author Mario Gomez
 *
 */
public final class BenchmarkResults implements Serializable {
	private static final long serialVersionUID = 1L;
	private String dataset; // eg. patterson, j30, j60
	private String type; //  type of benchmark: opt (optimal), hrs (heuristics), lb (lower bound)
	private BenchmarkSolution[] benchmarkSolutions;
	
	/**
	 * @param dataset
	 * @param type
	 * @param benchmarkSolutions
	 */
	public BenchmarkResults(String dataset, String type, BenchmarkSolution[] benchmarkSolutions) {
		this.dataset = dataset;
		this.type = type;
		this.benchmarkSolutions = benchmarkSolutions;
	}
		
	/**
	 * @return
	 */
	public String getDataset() {
		return dataset;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return
	 */
	public BenchmarkSolution[] getBenchmarkSolutions() {
		return benchmarkSolutions;
	}

	/**
	 * @return
	 */
	public int getNumProblems() {
		return benchmarkSolutions.length;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("\nBenchmark results for dataset ");
		s.append(dataset);
		s.append("\nProblem\tUB\tLB\tOptimal Solution\tBest Heuristic Solution\n");
		for (BenchmarkSolution solution : benchmarkSolutions) {
			s.append(solution);
			s.append("\n");
		}
		return s.toString();
	}

}
