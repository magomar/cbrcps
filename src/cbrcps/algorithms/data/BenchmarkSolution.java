package cbrcps.algorithms.data;

import java.io.Serializable;

/**
 * @author Mario Gomez
 *
 */
public final class BenchmarkSolution implements Serializable {
	private static final long serialVersionUID = 1L;
	private String problem;
	private int upperBound;
	private int lowerBound;
	/**
	 * Optimal solution
	 */
	private Solution optimalSolution;
	/**
	 * Best heuristic solution
	 */
	private Solution bestHeuristicSolution;
	

	/**
	 * @param problem
	 * @param lowerBound
	 * @param upperBound
	 */
	public BenchmarkSolution(String problem, int lowerBound, int upperBound) {
		this.problem = problem;
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
	}
	

	/**
	 * @param problem
	 * @param makespan
	 */
	public BenchmarkSolution(String problem, int makespan) {
		this.problem = problem;
		this.upperBound = makespan;
		this.lowerBound = makespan;
	}
	
	/**
	 * @return
	 */
	public String getProblem() {
		return problem;
	}

	/**
	 * @return
	 */
	public int getUpperBound() {
		return upperBound;
	}

	/**
	 * @return
	 */
	public int getLowerBound() {
		return lowerBound;
	}

	/**
	 * @return
	 */
	public Solution getOptimalSolution() {
		return optimalSolution;
	}

	/**
	 * @param solution
	 */
	public void setOptimalSolution(Solution solution) {
		this.optimalSolution = solution;
	}
	
	/**
	 * @return the bestHeuristicSolution
	 */
	public Solution getBestHeuristicSolution() {
		return bestHeuristicSolution;
	}

	/**
	 * @param bestHeuristicSolution the bestHeuristicSolution to set
	 */
	public void setBestHeuristicSolution(Solution bestHeuristicSolution) {
		this.bestHeuristicSolution = bestHeuristicSolution;
	}

	/**
	 * @return the makespan of the best solution
	 */
	public int getOptimalSolutionMakespan() {
		return optimalSolution.getMakespan();
	}

	/**
	 * @return true if best solution improves known upper bound
	 */
	public boolean improvesBenchmark() {
		return (optimalSolution.getMakespan() < upperBound);
	}

	/**
	 * @return
	 */
	public boolean equalsBenchmark() {
		return (optimalSolution.getMakespan() == upperBound);
	}

	/**
	 * @return
	 */
	public boolean equalsOrImprovesBenchmark() {
		return (optimalSolution.getMakespan() <= upperBound);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(problem);
		s.append("\t");
		s.append(upperBound);
		s.append("\t");
		s.append(lowerBound);
		s.append("\t");
		s.append(optimalSolution);
		s.append("\t");
		s.append(bestHeuristicSolution);
		return s.toString();
	}
	
	/**
	 * @param newSolution
	 * @return
	 */
	public double computeDeviation(Solution newSolution) {
		return (double) (newSolution.getMakespan() - lowerBound) / (double) lowerBound;
	}

//	public void initializeBenchmarkSolution(Project project, BenchmarkSolution benchmarkSolution) {
//		setProblem(benchmarkSolution.getProblem());
//		setUpperBound(benchmarkSolution.getUpperBound());
//		setLowerBound(benchmarkSolution.getLowerBound());
//		setSolution(new Solution(project.getNumJobs(), project.getTimeHorizon()));
//	}
}
