/**
 *
 */
package cbrcps.algorithms.scheduler.multiheuristic;

import java.util.Arrays;
import java.util.logging.Logger;

import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.heuristics.jobselection.JobPriorityRule;
import cbrcps.algorithms.heuristics.modeselection.ModePriorityRule;
import cbrcps.algorithms.sampler.DeterministicSampler;
import cbrcps.algorithms.sampler.Sampler;
import cbrcps.algorithms.scheduler.AbstractScheduler;
import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.algorithms.scheduler.serialgenerationscheme.SSGS;
import rcpsp.data.jaxb.Project;
import cbrcps.util.Constant;
import cbrcps.util.Stopwatch;

/**
 * @author Mario Gomez
 *
 */
public class MultiHeuristicSSGS implements Scheduler {
//	 Algorithm fields
	protected JobPriorityRule[] jobPriorityRule;
	protected ModePriorityRule[] modePriorityRule ;
	protected Sampler sampler;
	protected int iterations;
	protected Solution bestSolution;
	protected Project project;

	//	 Auxiliary fields
	protected final Logger logger = Logger.getLogger(this.getClass().getName());
	protected final Stopwatch stopwatch = new Stopwatch();

	/**
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 * @param sampler
	 * @param iterations
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule, ModePriorityRule[] modePriorityRule, Sampler sampler, int iterations) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = modePriorityRule;
		this.sampler = sampler;
		this.iterations = iterations;
		
	}
	
	/**
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule, ModePriorityRule[] modePriorityRule) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = modePriorityRule;
		this.sampler = new DeterministicSampler();
		this.iterations = 1;
	}
	
	/**
	 * @param jobPriorityRule
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = Constant.bestMode;
		this.sampler = new DeterministicSampler();
		this.iterations = 1;
	}
	
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.scheduler.Scheduler#solve(rcpsp.data.jaxb.Project)
	 */
	@Override
	public Solution solve(Project project) {
		stopwatch.start();
		this.project = project;
		bestSolution = new Solution(project.getNumJobs(), project.getTimeHorizon());
		for (int j = 0; j < jobPriorityRule.length; j++)
			for (int m = 0; m < modePriorityRule.length; m++) {
				SSGS solver = new SSGS(jobPriorityRule[j], modePriorityRule[m], sampler, iterations);
				Solution newSolution = solver.solve(project);
				if (newSolution.getMakespan() < bestSolution.getMakespan()) {
					bestSolution = newSolution;
				}
			}
		if (!bestSolution.isComplete()) logger.info(toString() + ": No solution found");
		return bestSolution;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MHS_" + sampler.toString() + Arrays.asList(jobPriorityRule).toString();
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.scheduler.Scheduler#newInstance()
	 */
	@Override
	public Scheduler newInstance() {
		JobPriorityRule[] newJobPriorityRule = new JobPriorityRule[jobPriorityRule.length];
		for (int i = 0; i < newJobPriorityRule.length; i++) {
			newJobPriorityRule[i] = (JobPriorityRule) jobPriorityRule[i].newInstance();
		}
		
		ModePriorityRule[] newModePriorityRule = new ModePriorityRule[modePriorityRule.length];
		for (int i = 0; i < newModePriorityRule.length; i++) {
			newModePriorityRule[i] = (ModePriorityRule) modePriorityRule[i].newInstance();
		}
		Sampler newSampler = sampler.newInstance();
		
		return new MultiHeuristicSSGS(newJobPriorityRule, newModePriorityRule, newSampler, iterations);
	}
	
	/* (non-Javadoc)
	 * @see cbrcps.algorithms.scheduler.Scheduler#getAlgorithm()
	 */
	@Override
	public String getAlgorithm() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see cbrcps.algorithms.scheduler.Scheduler#close()
	 */
	public void close() {
		stopwatch.stop();
		System.out.println("* Final solution for " + project.getName() + ":" + bestSolution.toString() + stopwatch.toString());
	}
}

