/**
 *
 */
package cbrcps.algorithms.scheduler.branchandbound;

import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.heuristics.jobselection.JobPriorityRule;
import cbrcps.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import cbrcps.util.Constant;
import java.util.Collections;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 */
public class SprecherDrexlHeuristicTimeLimited extends SprecherDrexlHeuristic {

    private static final Logger LOG = Logger.getLogger(SprecherDrexlHeuristicTimeLimited.class.getName());
    protected long timeLimit;

    /**
     * @param jobPriorityRule
     * @param timeLimit
     */
    public SprecherDrexlHeuristicTimeLimited(JobPriorityRule jobPriorityRule, long timeLimit) {
        super(jobPriorityRule);
        this.timeLimit = timeLimit;
    }

    /**
     * @param timeLimit
     */
    public SprecherDrexlHeuristicTimeLimited(long timeLimit) {
        this(Constant.bestRule[0], timeLimit);
    }


    /* (non-Javadoc)
     * @see cbrcps.algorithms.scheduler.branchandbound.SprecherDrexl#solve()
     */
    @Override
    public Solution solve(Project project) {
        init(project);

        //level 0
        selectedJob[0] = 0;

        //level 1;
        eligibleJobs[1].add(0);
        selectedJobIndex[1] = 0;
        selectedJob[1] = 0;

        scheduledJobs.add(0);
        startTime[0] = 0;
        finishTime[0] = 0;
        mode[0] = 0;

        level = 2;
        for (Integer successor : successors[0]) {
            eligibleJobs[level].add(successor);
        }

        //		Heuristic: sorts the set of (remaining) eligible jobs => determines the evaluation order)
        Collections.sort(eligibleJobs[level], jobPriorityRule);

        selectedJobIndex[level] = 0;
        selectedJob[level] = eligibleJobs[level].get(0).intValue();
        mode[selectedJob[level]] = -1;

        // Select next untested mode or descendant
        while (stopwatch.getTotalTime() < timeLimit || !bestSolution.isComplete()) {
            if (mode[selectedJob[level]] < modes[selectedJob[level]].size() - 1) {
                mode[selectedJob[level]]++;
                findFeasibleStartTime();
            } else if (selectedJobIndex[level] < eligibleJobs[level].size() - 1) {
                selectedJobIndex[level]++;
                selectedJob[level] = eligibleJobs[level].get(selectedJobIndex[level]).intValue();
                mode[selectedJob[level]] = 0;
                findFeasibleStartTime();
            } else { // One level backtracking [Step 3]
                level--;
                if (level == 0) { // Finish !
                    break;
                } else { // unschedule previously scheduled job
                    int jobToUnSchedule = selectedJob[level];
                    scheduledJobs.remove(scheduledJobs.size() - 1);
                    Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                    int resourceID = 0;
                    for (Integer request : jobMode.getResourceRequest()) {
                        for (int k = 1; k <= level; k++) {
                            if (resource[resourceID].isRenewable()) {
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                        && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            } else {
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            }
                        }
                        resourceID++;
                    }
                }
            }
            stopwatch.stop();
        }

        if (!bestSolution.isComplete()) {
            LOG.info("No solution found");
        } else {
//			stopwatch.stop();
//			System.out.println(toString() +" > Solution found " + bestSolution.toString());
//			System.out.println(stopwatch.toString() + "   Nodes visited : " + nodesVisited + 
//			"   Nodes explored: " + nodesExplored +  "   Nodes scheduled: " + nodesScheduled + "   Time per node: " + 
//			(float) stopwatch.getTotalTime()/nodesVisited);
//			System.out.println();
        }
        return bestSolution;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SDHTL: " + jobPriorityRule.toString();
    }

    /* (non-Javadoc)
     * @see cbrcps.algorithms.scheduler.Scheduler#newInstance()
     */
    @Override
    public Scheduler newInstance() {
        JobPriorityRule newJobPriorityRule = (JobPriorityRule) jobPriorityRule.newInstance();
        return new SprecherDrexlHeuristicTimeLimited(newJobPriorityRule, timeLimit);
    }
}
