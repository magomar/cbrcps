package cbrcps.util;

import cbrcps.algorithms.heuristics.jobselection.DFS;
import cbrcps.algorithms.heuristics.jobselection.EFT;
import cbrcps.algorithms.heuristics.jobselection.EST;
import cbrcps.algorithms.heuristics.jobselection.GRPW;
import cbrcps.algorithms.heuristics.jobselection.JN;
import cbrcps.algorithms.heuristics.jobselection.JobPriorityRule;
import cbrcps.algorithms.heuristics.jobselection.LFT;
import cbrcps.algorithms.heuristics.jobselection.LPT;
import cbrcps.algorithms.heuristics.jobselection.LST;
import cbrcps.algorithms.heuristics.jobselection.MSLK;
import cbrcps.algorithms.heuristics.jobselection.MTS;
import cbrcps.algorithms.heuristics.jobselection.MultiPriorityRule;
import cbrcps.algorithms.heuristics.jobselection.NIS;
import cbrcps.algorithms.heuristics.jobselection.SPT;
import cbrcps.algorithms.heuristics.jobselection.TRD;
import cbrcps.algorithms.heuristics.jobselection.TRS;
import cbrcps.algorithms.heuristics.jobselection.WRUP;
import cbrcps.algorithms.heuristics.modeselection.ModePriorityRule;
import cbrcps.algorithms.heuristics.modeselection.SFM;
import cbrcps.algorithms.sampler.AbstractSampler;
import cbrcps.algorithms.sampler.ModifiedRBRS;


/**
 * @author Mario Gomez
 *
 */

public final class Constant {
	public final static String datasetsPath = "datasets/";
	public final static String resultsPath = "results/";
	public final static String sqlPath = "src/cbrcps/cbr/database/";

	private final static JobPriorityRule[][] mr = {
		{new EST(), new MSLK()},
		{new EST(), new LFT()},
		{new EST(), new LST()},
		{new EST(), new LFT(), new MSLK()},
		{new EST(), new JN()},
		{new EST(), new MTS()},
		{new EST(), new NIS()},
		{new EST(), new WRUP()}

	};
	private final static  double[][] wmr = {
		{0.80, 0.20},
		{0.80, 0.20},
		{0.80, 0.20},
		{0.9, 0.09, 0.01},
		{0.9, 0.10},
		{0.99, 0.01},
		{0.80, 0.20},
		{0.80, 0.20},
	};

	public static JobPriorityRule[] bestRule = {
		new MultiPriorityRule(mr[0], wmr[0]),
		new MultiPriorityRule(mr[1], wmr[1]),
		new MultiPriorityRule(mr[2], wmr[2]),
		new MultiPriorityRule(mr[3], wmr[3]),
		new MultiPriorityRule(mr[4], wmr[4]),
		new MultiPriorityRule(mr[5], wmr[5]),
		new MultiPriorityRule(mr[6], wmr[6]),
//		new EST(),
		new LST(),
//		new DFS(),
//		new TRS(),
		new LFT(),
//		new WRUP(),
		new MTS(),
	};

	public final static JobPriorityRule[] singleRule = {
		new LST(),
		new LFT(),
		new EST(),
		new EFT(),
		new WRUP(),
		new MTS(),
		new MSLK(),
		new TRS(),
		new NIS(),
		new JN(),
		new GRPW(),
		new LPT(),
		new SPT(),
		new TRD(),
		new DFS(),
	};

	public final static ModePriorityRule[] bestMode = {
		new SFM(),
	};

	public final static int[] iterations = {
		500, 
		1000,
		5000
		};
	public final static double epsilon = 1;
	public final static double[] alpha = {
//		12, 13, 14,
		15
		};
	public final static double[] lambda = {10};

	public final static AbstractSampler bestSampler = new ModifiedRBRS(10, 10);

	/**
	 * @param ruleName the name of a priority rule
	 * @return the priority rule with the given name
	 */
	public static JobPriorityRule getPriorityRuleByName(String ruleName) {
		JobPriorityRule result = null;
		for (JobPriorityRule rule : singleRule) {
			if (rule.toString().equals(ruleName)) {
				result = rule;
				break;
			}
		}

		return result;
	}
}
