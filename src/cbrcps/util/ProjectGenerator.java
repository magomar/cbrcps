/**
 *
 */
package cbrcps.util;

import cbrcps.algorithms.data.ProjectDescription;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 */
public final class ProjectGenerator {

    private static final Random random = new Random(System.currentTimeMillis());

    /**
     * Creates a new Project with a random number of jobs removed. The number of jobs removed is at least 1 and no more
     * than maxJobsToRemove
     *
     * @param project
     * @param maxJobsToRemove
     * @return a new project
     */
    public static Project removeJobs(Project project, int maxJobsToRemove) {
        Project newProject = ProjectUtil.clone(project);

        int size = newProject.getNumJobs();
        int numJobsToRemove = random.nextInt(maxJobsToRemove) + 1;
        int currentSize = size;
        int timeHorizon = newProject.getTimeHorizon();
        for (int i = 0; i < numJobsToRemove; i++) {
            int randomJob = random.nextInt(currentSize - 2) + 1; //random job excluding dummy start/end jobs
            List<Job> jobs = newProject.getJob();
            Job jobToRemove = jobs.get(randomJob);
            jobs.remove(jobToRemove);
            timeHorizon -= jobToRemove.getMode().get(0).getDuration();
            currentSize--;
            // add successors of removed job to successors of its predecessors
            Integer jobToRemoveID = jobToRemove.getId();
            for (Job job : jobs) {
                List<Integer> jobSuccessors = job.getSuccessor();
                if (jobSuccessors.contains(jobToRemoveID)) {
                    for (Integer successor : jobToRemove.getSuccessor()) {
                        if (!jobSuccessors.contains(successor)) {
                            jobSuccessors.add(successor);
                        }
                    }
                }
            }
            for (Job job : jobs) {
                if (job.getId() > randomJob) {
                    job.setId(job.getId() - 1);
                }
                ArrayList<Integer> newJobSuccessors = new ArrayList<>();
                for (Integer successor : job.getSuccessor()) {
                    int succ = successor.intValue();
                    if (succ > randomJob) {
                        newJobSuccessors.add(succ - 1);
                    } else if (succ < randomJob) {
                        newJobSuccessors.add(successor);
                    }
                }
                Collections.sort(newJobSuccessors);
                job.getSuccessor().clear();
                job.getSuccessor().addAll(newJobSuccessors);
                job.setNumSuccessors(newJobSuccessors.size());
            }
        }
        newProject.setNumJobs(currentSize);
        newProject.setTimeHorizon(timeHorizon);

        // remove the successors of the start job that have more than one predecessor 
        ProjectDescription pd = new ProjectDescription(newProject);
        List<Integer>[] predecessors = pd.getPredecessors();
        List<Integer> newStartSuccessors = new ArrayList<Integer>();
        Job startJob = newProject.getJob().get(0);
        for (Integer successor : startJob.getSuccessor()) {
            if (predecessors[successor.intValue()].size() == 1) {
                newStartSuccessors.add(successor);
            }
        }
        startJob.getSuccessor().clear();
        startJob.getSuccessor().addAll(newStartSuccessors);
        startJob.setNumSuccessors(newStartSuccessors.size());

        // remove the end job from successors of jobs having more than 1 successor
        pd = new ProjectDescription(newProject);
        predecessors = pd.getPredecessors();
        Job endJob = newProject.getJob().get(newProject.getNumJobs() - 1);
        for (Integer predecessor : predecessors[endJob.getId()]) {
            Job job = newProject.getJob().get(predecessor.intValue());
            if (job.getNumSuccessors() > 1) {
                job.getSuccessor().remove(Integer.valueOf(endJob.getId()));
                job.setNumSuccessors(job.getNumSuccessors() - 1);
            }
        }

//		System.out.println("Initial size: " + size + "; New Size: " + currentSize + "; Jobs removed: " + numJobsToRemove);
        return newProject;
    }

    /**
     * @param project
     * @param numJobsToRemove
     * @return
     */
    public static Project removeNJobs(Project project, int numJobsToRemove) {
        Project newProject = ProjectUtil.clone(project);

        int size = newProject.getNumJobs();
        int currentSize = size;
        int timeHorizon = newProject.getTimeHorizon();
        for (int i = 0; i < numJobsToRemove; i++) {
            int randomJob = random.nextInt(currentSize - 2) + 1; //random job excluding dummy start/end jobs
            List<Job> jobs = newProject.getJob();
            Job jobToRemove = jobs.get(randomJob);
            jobs.remove(jobToRemove);
            timeHorizon -= jobToRemove.getMode().get(0).getDuration();
            currentSize--;
            // add successors of removed job to successors of its predecessors
            Integer jobToRemoveID = jobToRemove.getId();
            for (Job job : jobs) {
                List<Integer> jobSuccessors = job.getSuccessor();
                if (jobSuccessors.contains(jobToRemoveID)) { // if job is a predecessor of jobToRemove
                    for (Integer successor : jobToRemove.getSuccessor()) {
                        if (!jobSuccessors.contains(successor)) {
                            jobSuccessors.add(successor);
                        }
                    }
                }
            }
            for (Job job : jobs) {
                if (job.getId() > randomJob) {
                    job.setId(job.getId() - 1);
                }
                List<Integer> newJobSuccessors = new ArrayList<Integer>();
                for (Integer successor : job.getSuccessor()) {
                    int succ = successor.intValue();
                    if (succ > randomJob) {
                        newJobSuccessors.add(Integer.valueOf(succ - 1));
                    } else if (succ < randomJob) {
                        newJobSuccessors.add(successor);
                    }
                }
                Collections.sort(newJobSuccessors);
                job.getSuccessor().clear();
                job.getSuccessor().addAll(newJobSuccessors);
                job.setNumSuccessors(newJobSuccessors.size());
            }
        }
        newProject.setNumJobs(currentSize);
        newProject.setTimeHorizon(timeHorizon);

        // remove the successors of the start job that have more than one predecessor 
        ProjectDescription pd = new ProjectDescription(newProject);
        List<Integer>[] predecessors = pd.getPredecessors();
        List<Integer> newSuccessors = new ArrayList<>();
        Job startJob = newProject.getJob().get(0);
        for (Integer successor : startJob.getSuccessor()) {
            if (predecessors[successor.intValue()].size() == 1) {
                newSuccessors.add(successor);
            }
        }
        startJob.getSuccessor().clear();
        startJob.getSuccessor().addAll(newSuccessors);
        startJob.setNumSuccessors(newSuccessors.size());

        // finally, remove the end job from successors of jobs having more than 1 successor
        pd = new ProjectDescription(newProject);
        predecessors = pd.getPredecessors();
        Job endJob = newProject.getJob().get(newProject.getNumJobs() - 1);
        for (Integer predecessor : predecessors[endJob.getId()]) {
            Job job = newProject.getJob().get(predecessor.intValue());
            if (job.getNumSuccessors() > 1) {
                job.getSuccessor().remove(Integer.valueOf(endJob.getId()));
                job.setNumSuccessors(job.getNumSuccessors() - 1);
            }
        }

//		System.out.println("Initial size: " + size + "; New Size: " + currentSize + "; Jobs removed: " + numJobsToRemove);
        return newProject;
    }

    /**
     * @param project
     * @return
     */
    public static Project reduceResources(Project project) {
        Project newProject = ProjectUtil.clone(project);
        int numResources = project.getResource().size();
        int[] maxRequest = new int[numResources];
        int[] resourceCapacity = new int[numResources];
        List<Resource> resources = project.getResource();

        int r = 0;
        for (Resource resource : resources) {
            resourceCapacity[r] = resource.getCapacity();
            maxRequest[r] = 0;
            r++;
        }

        for (Job j : project.getJob()) {
            int[] maxPartialRequest = new int[numResources];
            for (r = 0; r < numResources; r++) {
                maxPartialRequest[r] = 0;
            }
            for (Mode m : j.getMode()) {
                for (r = 0; r < numResources; r++) {
                    maxPartialRequest[r] = Math.max(maxPartialRequest[r], m.getResourceRequest().get(r));
                }
            }
            for (r = 0; r < project.getNumRenewableResources(); r++) {
                maxRequest[r] = Math.max(maxRequest[r], maxPartialRequest[r]);
            }
            for (r = project.getNumRenewableResources(); r < numResources; r++) {
                maxRequest[r] = maxRequest[r] + maxPartialRequest[r];
            }

        }
        // reduce renewable resources to maxRequest

        List<Resource> newResources = new ArrayList<>();
        r = 0;
        for (Resource resource : resources) {
            Resource newResource = ProjectUtil.clone(resource);
            if (maxRequest[r] < resourceCapacity[r]) {
                newResource.setCapacity(maxRequest[r]);
//				System.out.println("Reducing resource capacity from " + resourceCapacity[r] + " to " + maxRequest[r]);
            }
            newResources.add(newResource);
            r++;
        }
        newProject.getResource().clear();
        newProject.getResource().addAll(newResources);
        return newProject;
    }

    /**
     * @param project
     * @param maxSuccessors
     * @return
     */
    public static Project removePrecedenceRelations(Project project, int maxSuccessors) {
        Project newProject = ProjectUtil.clone(project);

        int totalSucessorsRemoved = 0;
        int targetSuccessors = random.nextInt(maxSuccessors) + 1;
        for (Job sourceJob : newProject.getJob()) {
            ProjectDescription pd = new ProjectDescription(newProject); // used to obtain the predecessors
            List<Integer> successors = sourceJob.getSuccessor();
            // only those successors having more than one predecessor can be removed
            // if we removed succesors with only one predecessor (that is, the source job)
            // then we would need to add the successor to the successors of the dummy start job,
            // and we do not want that
            List<Integer> validSuccessors = new ArrayList<Integer>();
            for (Integer successor : successors) {
                if (pd.getPredecessors()[successor.intValue()].size() > 1) {
                    validSuccessors.add(successor);
                }
            }
            int successorsToRemove = Math.min(successors.size() - targetSuccessors, validSuccessors.size());
            if (successorsToRemove > 0) {
                totalSucessorsRemoved += successorsToRemove;
                Collections.shuffle(validSuccessors);
                for (int i = 0; i < successorsToRemove; i++) {
                    successors.remove(validSuccessors.get(0));
                    validSuccessors.remove(Integer.valueOf(0));
                }
                sourceJob.setNumSuccessors(successors.size());
            }

        }
        System.out.println("Precedence relations removed: " + totalSucessorsRemoved);
        return newProject;
    }
}
