/**
 *
 */
package cbrcps.experiment;

import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Project;
import static cbrcps.util.Report.reportSolutions;
import static cbrcps.util.Report.reportStatistics;
import cbrcps.wrappers.DatasetWrapper;

/**
 * @author Mario Gomez
 *
 */
public final class CompareAlgorithmsConcurrently extends AbstractExperiment {

    private final Scheduler[] scheduler;
    private final DatasetWrapper wrapper;

    /**
     * @param name
     * @param scheduler
     * @param wrapper
     */
    public CompareAlgorithmsConcurrently(String name, Scheduler[] scheduler, DatasetWrapper wrapper) {
        super(name);
        this.scheduler = scheduler;
        this.wrapper = wrapper;
    }

    @Override
    public void runExperiment() {
        super.runExperiment();

        String[] dataset = wrapper.getDatasetName();
        int numDatasets = dataset.length;
        int numAlgorithms = scheduler.length;

        double[][][] data = new double[dataset.length][numAlgorithms][];
        BenchmarkResults[] newBenchmark = new BenchmarkResults[numDatasets];
        BenchmarkSolution[][] newBenchmarkSolution = new BenchmarkSolution[numDatasets][];
        String[] algorithm = new String[numAlgorithms];

        for (int a = 0; a < numAlgorithms; a++) {
            algorithm[a] = scheduler[a].getAlgorithm();
        }

        for (int d = 0; d < numDatasets; d++) {
            BenchmarkResults benchmark = wrapper.parseBenchmarkFile(dataset[d]);
            int numProblems = benchmark.getNumProblems();
            for (int a = 0; a < numAlgorithms; a++) {
                data[d][a] = new double[numProblems];
            }
            Project[] project = wrapper.loadDatasetProjects(benchmark);
            newBenchmarkSolution[d] = new BenchmarkSolution[numProblems];
            int problemIndex = 0;
            for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
                newBenchmarkSolution[d][problemIndex] = new BenchmarkSolution(
                        benchmarkSolution.getProblem(),
                        benchmarkSolution.getLowerBound(),
                        benchmarkSolution.getUpperBound());
                newBenchmarkSolution[d][problemIndex].setOptimalSolution(new Solution(project[problemIndex].getNumJobs(), project[problemIndex].getTimeHorizon() + 1));
                problemIndex++;
            }

            // EXECUTE SCHEDULING ALGORITHMS
            Solution[][] newSolution = new Solution[numAlgorithms][];
            for (int a = 0; a < numAlgorithms; a++) {
                newSolution[a] = TaskScheduler.solveMultipleProblemsConcurrently(scheduler[a], project);
            }

            // determine which is the best solution among the solutions obtained by all algorithms
            problemIndex = 0;
            for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
                for (int a = 0; a < numAlgorithms; a++) {
                    data[d][a][problemIndex] = benchmarkSolution.computeDeviation(newSolution[a][problemIndex]);
                    if (newSolution[a][problemIndex].getMakespan() < newBenchmarkSolution[d][problemIndex].getOptimalSolutionMakespan()) {
                        newBenchmarkSolution[d][problemIndex].setOptimalSolution(newSolution[a][problemIndex]);
                    }
                }
                problemIndex++;
            }
            newBenchmark[d] = new BenchmarkResults(benchmark.getDataset(), benchmark.getType(), newBenchmarkSolution[d]);
        }

        // print results
        reportStatistics(algorithm, dataset, data, this.toString());
        reportSolutions(newBenchmark, dataset, this.toString());
    }
}
