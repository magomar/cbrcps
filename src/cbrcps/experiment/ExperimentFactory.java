/**
 * 
 */
package cbrcps.experiment;

import cbrcps.algorithms.heuristics.jobselection.AbstractJobPriorityRule;
import cbrcps.algorithms.heuristics.jobselection.CBPR;
import cbrcps.algorithms.heuristics.jobselection.CaseBasedPriorityRule;
import cbrcps.algorithms.heuristics.jobselection.JobPriorityRule;
import cbrcps.algorithms.matcher.ProjectMatcher;
import cbrcps.algorithms.matcher.SimPackMatcher;
import cbrcps.algorithms.sampler.ModifiedRBRS;
import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.algorithms.scheduler.multiheuristic.MultiHeuristicSSGS;
import cbrcps.algorithms.scheduler.serialgenerationscheme.SSGS;
import cbrcps.algorithms.scheduler.serialgenerationscheme.SSGS_CBR;
import cbrcps.cbr.CBRCPS;
import cbrcps.util.Constant;
import cbrcps.util.MultiRuleGenerator;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;
import jcolibri.exception.ExecutionException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 */
public final class ExperimentFactory {
    private static final Logger LOG = Logger.getLogger(ExperimentFactory.class.getName());
	
	
	/**
	 * @param jobPriorityRule
	 * @return
	 */
	public static Scheduler[] getAlgorithmsToCompareCBR(JobPriorityRule jobPriorityRule) {
		final CaseBasedPriorityRule cbpr = new CBPR(jobPriorityRule);
//		final CaseBasedPriorityRule cbpr = new CBPR_AR(jobPriorityRule);
		// CBR wrapper: accesses the problems referred by the cases in the case_base
		DatasetWrapper cbWrapper = WrapperFactory.obtainWrapper(WrapperFactory.casebase);
//		ProjectMatcher matcher = new BasicMatcher();
//		ProjectMatcher matcher = new BasicMatcher2();
//		ProjectMatcher matcher = new AbsurdistMatcher();
		ProjectMatcher matcher = new SimPackMatcher();
		int[] k = {
//				1,3,
				5,
//				10,
//				20
				};
		CBRCPS[] cbrcps = new CBRCPS[k.length];
		for (int i = 0; i < cbrcps.length; i++) {
			cbrcps[i] = new CBRCPS(k[i]);
                try {
                    cbrcps[i].configure();
                } catch (ExecutionException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
                try {
                    cbrcps[i].preCycle();
                } catch (ExecutionException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
			
		}
		Scheduler[] scheduler = new Scheduler[cbrcps.length + 1];
		scheduler[0] = new SSGS(jobPriorityRule, Constant.bestMode[0]);
		for (int i = 0; i < cbrcps.length; i++) {
			scheduler[i+1] = new SSGS_CBR(cbpr, jobPriorityRule, cbrcps[i], cbWrapper, matcher);
		}
		return scheduler;

	}


	/**
	 * @return
	 */
	public static Scheduler[] getAlgorithmsToCompareHeuristics() {
		// Compare Heuristics
		// Parameters used to combine rules
		// weights used to compose multi-rules
		double[] ruleWeight = {1}; // lenght of ruleWeight determines the number of rules used to build multi-rules
		boolean useWeightPermutations = true;
		// for example, if we want to compare multiheuristics made of two rules and same weight, 
		// then we just need set ruleWeight = {0.5,0.5} and useWeightPermutations = false
		// if we want to give more weight to one rule over another, then ruleWeight = {0.25,0.75} and useWeightPermutations = true
//		JobPriorityRule[] multiRule = MultiRuleGenerator.getMultiRules(Constant.singleRule, ruleWeight, useWeightPermutations);
		JobPriorityRule[] multiRule = Constant.singleRule;
		Scheduler[] scheduler = new Scheduler[multiRule.length * Constant.bestMode.length];
		int index = 0;
		for (int i = 0; i < multiRule.length; i++) {
			for (int j = 0; j < Constant.bestMode.length; j++) {
				scheduler[index] = new SSGS(multiRule[i], Constant.bestMode[j]);
				index++;
			}
		}
		return scheduler;
	}

	/**
	 * @return
	 */
	public static Scheduler[] getAlgorithmsToCompareBestRules() {
		JobPriorityRule[][] multiRuleList = new AbstractJobPriorityRule[Constant.bestRule.length][];
		for (int i = 0; i < Constant.bestRule.length; i++) {
			multiRuleList[i] = new JobPriorityRule[i+1];
			for (int j = 0; j < i + 1; j++) {
				multiRuleList[i][j] = Constant.bestRule[j];
			}
		}

		Scheduler[] scheduler = new Scheduler[multiRuleList.length];
		for (int i = 0; i < multiRuleList.length; i++) {
			scheduler[i] = new MultiHeuristicSSGS(multiRuleList[i], Constant.bestMode);
		}


		return scheduler;
	}

	/**
	 * @return
	 */
	public static Scheduler[] getAlgorithmsToCompareMultiHeuristic() {
		// weights used to combine priority rules
		// final double[] ruleWeight = {5, 2, 1};
		double[] ruleWeight = {1};
		boolean useWeightPermutations = true;
		int multiRuleListSize = 3;

		JobPriorityRule[][] multiRuleList = MultiRuleGenerator.getMultiRuleLists(Constant.singleRule, ruleWeight, useWeightPermutations, multiRuleListSize);

		Scheduler[] scheduler = new Scheduler[multiRuleList.length];
		for (int i = 0; i < multiRuleList.length; i++) {
			scheduler[i] = new MultiHeuristicSSGS(multiRuleList[i],Constant.bestMode);
		}

		return scheduler;
	}

	/**
	 * @return
	 */
	public static Scheduler[] getAlgorithmsToCompareSampling() {
		final Scheduler[] scheduler = new Scheduler[Constant.iterations.length * Constant.alpha.length * Constant.lambda.length];
		int index = 0;
		for (int i = 0; i < Constant.iterations.length; i++)
			for (int j = 0; j< Constant.alpha.length; j++)
				for (int k = 0; k < Constant.lambda.length; k++) {
					scheduler[index] = new SSGS(Constant.bestRule[0], Constant.bestMode[0], 
							new ModifiedRBRS(Constant.alpha[j], Constant.lambda[k]), Constant.iterations[i]);
					index++;
				}

		return scheduler;
	}

}
