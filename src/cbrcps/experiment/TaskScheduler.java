/**
 * 
 */
package cbrcps.experiment;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Project;
import cbrcps.multithread.SolveProblemExecutor;

/**
 * @author Mario Gomez
 *
 */
public final class TaskScheduler {
	

	/**
	 * @param scheduler
	 * @param project
	 * @return
	 */
	public static Solution[] solveMultipleProblems(Scheduler scheduler, Project[] project) {
		int numProblems = project.length;
		Solution[] solution = new Solution[numProblems];
		for (int p = 0; p < numProblems; p++) {
			solution[p] = scheduler.solve(project[p]);
			scheduler.close();
		}
		return solution;
	}

	/**
	 * @param scheduler
	 * @param project
	 * @return
	 */
	public static Solution[] solveMultipleProblemsConcurrently(Scheduler scheduler, Project[] project) {
		ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		int numProblems = project.length;
		Solution[] solution = new Solution[numProblems];
		Future<Solution>[] future = new Future[numProblems];
		for (int p = 0; p < numProblems; p++) {
			SolveProblemExecutor solver = new SolveProblemExecutor(scheduler.newInstance(), project[p]);
			future[p] = executorService.submit(solver);
		}

		for (int p = 0; p < numProblems; p++) {
			try {
				solution[p] = future[p].get();
			} catch (InterruptedException | ExecutionException e) {
			}
		}
		return solution;
	}
}
