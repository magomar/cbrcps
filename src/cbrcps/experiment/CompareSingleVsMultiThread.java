/**
 * 
 */
package cbrcps.experiment;

import java.text.NumberFormat;
import java.util.Locale;

import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Project;
import cbrcps.wrappers.DatasetWrapper;

/**
 * @author Mario Gomez
 *
 */
public final class CompareSingleVsMultiThread extends AbstractExperiment {
	private final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
	private final Scheduler scheduler;
	private final DatasetWrapper wrapper;

	/**
	 * @param name
	 * @param scheduler
	 * @param wrapper
	 */
	public CompareSingleVsMultiThread(String name, Scheduler scheduler, DatasetWrapper wrapper) {
		super(name);
		this.scheduler = scheduler;
		this.wrapper = wrapper;
	}

	/* (non-Javadoc)
	 * @see cbrcps.experiment.AbstractExperiment#runExperiment()
	 */
	@Override
	public void runExperiment() {
		super.runExperiment();
		
		long singleThreadTimeCost;
		long multiThreadTimeCost;

		String[] datasetName = wrapper.getDatasetName();		
		int numDatasets = datasetName.length;

		Project[][] project = new Project[numDatasets][];

		for (int d = 0; d < numDatasets; d++) {
			BenchmarkResults benchmark  = wrapper.parseBenchmarkFile(datasetName[d]);
			int numProblems = benchmark.getNumProblems();
			project[d] = new Project[numProblems];
			int problemIndex = 0;
			for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
				String problem = benchmarkSolution.getProblem();
				project[d][problemIndex] = wrapper.parseProblem(datasetName[d], problem);
				System.out.print(".");
				problemIndex++;
			}
			System.out.println();
		}

		// MULTI-THREAD
		System.out.println("RUNNING MULTI-THREAD ");
		stopwatch.stop();
		for (int d = 0; d < numDatasets; d++) 
			TaskScheduler.solveMultipleProblemsConcurrently(scheduler, project[d]);
		multiThreadTimeCost = stopwatch.getTimeFromLastStop();
		System.out.println("FINISHED MULTI THREAD " + multiThreadTimeCost);
		System.out.println();
		
		// SINGLE THREAD
		System.out.println("RUNNING SINGLE THREAD ");
		stopwatch.stop();
		for (int d = 0; d < numDatasets; d++) 
			TaskScheduler.solveMultipleProblems(scheduler, project[d]);
		singleThreadTimeCost = stopwatch.getTimeFromLastStop();
		System.out.println("FINISHED SINGLE THREAD " + singleThreadTimeCost);
		System.out.println();
		
	
	

		double multiThreadImprovement = (double) multiThreadTimeCost / singleThreadTimeCost;
		System.out.println("Multi-Thread's cost is " + nf.format(multiThreadImprovement) + " times the cost of Single-Thread");
		
	}

}
