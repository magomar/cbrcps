package cbrcps.wrappers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author Mario Gomez
 *
 */
public final class WrapperFactory {

    // Private fields used to define specific wrappers (instances of the DatasetWrapper class)
    //	PSPLib Datasets
    static private final String[] sm_dataset = {
        "j30", //		"j60",
    //		"j90",
    //		"j120"
    };
    static private final String[] mm_dataset = {
        "j10", "j12", "j14", "j16", //		"j18",
    //		"j20",
    //		"j30",
    //		"m1","m2","m4","m5",
    //		"n0","n1","n3",
    //		"r1","r3","r4","r5",
    //		"c15","c21"
    };
    static private final String[] sm_type = {"opt", "lb", "lb", "lb"};
    static private final String[] mm_type = {"opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt",
        "opt", "opt", "opt", "opt", "opt", "opt", "opt", "hrs"};
    /**
     * Patterson Dataset
     */
    static private final String[] pat_dataset = {"pat"};
    /**
     * Fixed Patterson's dataset
     */
    static private final String[] pat_fix = {"pat_fix"};
    static private final String[] pat_type = {"opt"};
    /**
     * Datasets derived from Patterson's dataset
     */
    static private final String[] pat_test_dataset_a = {
        "pat1", "pat2",
        "pat3", "pat4",
        "pat5",
        "pat6", "pat7",
        "pat8", "pat9"
    };
    /**
     * More datasets derived from the Patterson dataset
     */
    static private final String[] pat_test_dataset_b = {
        "pat1", "pat2",
        "pat3", "pat4",
        "pat5",
        "pat6", "pat7",
        "pat8", "pat9"
    };
    static private final String[] pat_test_dataset_i = {
        "patmax5c", "patmax10c"
    };
    /**
     * CBR dataset - Dataset used by SSGS_CBR solver
     */
    static private final String[] cbr_dataset = {
        "pat5", //		"patmax5a",
    //		"patmax5b",
    //		"patmax5c"
    //		"patmax10a",
    //		"patmax10b",
    //		"patmax10c"
    };
    
    static private final String[] example_cbr_dataset = {"small"};
    
    /**
     * Some small examples
     */
    static private final String[] example_dataset = {"small"};
    static private final String[] opt_type = {"opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt", "opt"};
    // Global constants, used to obtain a specific dataset wrapper by calling obtainWrapper()
    static public final DatasetDefinition psplib_sm = new DatasetDefinition("psplib/rcpsp", sm_dataset, sm_type, ".sm", ".sm", "cbrcps.wrappers.PSPLibWrapper");
    static public final DatasetDefinition psplib_mm = new DatasetDefinition("psplib/mrcpsp", mm_dataset, mm_type, ".mm", ".mm", "cbrcps.wrappers.PSPLibWrapper");
    static public final DatasetDefinition patterson = new DatasetDefinition("patterson", pat_dataset, pat_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition patterson_test_a = new DatasetDefinition("patterson_test_a", pat_test_dataset_a, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition patterson_test_b = new DatasetDefinition("patterson_test_b", pat_test_dataset_b, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition patterson_test_i = new DatasetDefinition("patterson_test_i", pat_test_dataset_i, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition casebase = new DatasetDefinition("casebase", cbr_dataset, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition example_casebase = new DatasetDefinition("casebase", example_cbr_dataset, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition patterson_fix = new DatasetDefinition("patterson_fix", pat_fix, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");
    static public final DatasetDefinition examples = new DatasetDefinition("examples", example_dataset, opt_type, ".sm", ".rcp", "cbrcps.wrappers.RCPWrapper");

    static public DatasetWrapper obtainWrapper(DatasetDefinition dataset) {
        DatasetWrapper datasetWrapper = null;
        String wrapperClassName = dataset.getWrapperClass();
        try {
            Class wrapperClass = Class.forName(wrapperClassName);
            Class partypes[] = new Class[1];
            partypes[0] = DatasetDefinition.class;
            Constructor wrapperConstructor;
            wrapperConstructor = wrapperClass.getConstructor(partypes);
            Object arglist[] = new Object[1];
            arglist[0] = dataset;
            datasetWrapper = (DatasetWrapper) wrapperConstructor.newInstance(arglist);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SecurityException | NoSuchMethodException | InvocationTargetException e) {
        }
        return datasetWrapper;
    }
}
