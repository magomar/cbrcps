package cbrcps.wrappers;

import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.util.Constant;
import cbrcps.util.ProjectUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractDatasetWrapper implements DatasetWrapper {

    protected final String path;
    protected final String[] datasetName;
    protected final String[] benchmarkType;
    protected final String benchmarkFormat;
    protected final String problemFormat;
    private final DatasetDefinition datasetDefinition;

    /**
     * @param datasetDefinition
     */
    public AbstractDatasetWrapper(DatasetDefinition datasetDefinition) {
        this.path = Constant.datasetsPath + datasetDefinition.getPath() + "/";
        this.datasetName = datasetDefinition.getDataset();
        this.benchmarkType = datasetDefinition.getBenchmarkType();
        this.benchmarkFormat = datasetDefinition.getBenchmarkFormat();
        this.problemFormat = datasetDefinition.getProblemFormat();
        this.datasetDefinition = datasetDefinition;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#getDatasetNames()
     */
    @Override
    public String[] getDatasetName() {
        return datasetName;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#getPath()
     */
    @Override
    public DatasetDefinition getDatasetDefinition() {
        return datasetDefinition;
    }

    /**
     * @param datasetName
     * @return
     */
    protected int getDatasetIndex(String datasetName) {
        int datasetIndex = -1;
        for (int i = 0; i < this.datasetName.length; i++) {
            if (this.datasetName[i].equals(datasetName)) {
                datasetIndex = i;
                break;
            }
        }
        return datasetIndex;
    }

    /**
     * @param benchmark
     * @return
     */
    @Override
    public Project[] loadDatasetProjects(BenchmarkResults benchmark) {
        print("Loading " + benchmark.getDataset() + " projects");
        Project[] project = new Project[benchmark.getNumProblems()];
        ArrayList<String> incorrectProjects = new ArrayList<>();
        int problemIndex = 0;
        for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
            project[problemIndex] = parseProblem(benchmark.getDataset(), benchmarkSolution.getProblem());
            System.out.print(".");
            if (!ProjectUtil.checkCorrectness(project[problemIndex])) {
                incorrectProjects.add(project[problemIndex].getName());
            }
            problemIndex++;
        }
        System.out.println();
        print("Number of projects loaded: " + problemIndex);
        if (incorrectProjects.size() > 0) {
            print("There are " + incorrectProjects.size() + " incorrect projects !");
            print("Incorrect projects are " + incorrectProjects.toString());
        }

        return project;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#generateBenchmark(java.util.ArrayList, java.lang.String)
     */
    @Override
    public void generateBenchmark(List<Project> newProjects, String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
        StringBuilder s = new StringBuilder();
        s.append("=============================\n");
        s.append("Instance Makespan\n");
        s.append("------------------------------\n");
        int index = 1;
        for (Project project : newProjects) {
            s.append(index);
            s.append("\t");
            //s.append(0); // set makespan to 0, meaning the optimal makespan is "unknown"
            s.append(project.getTimeHorizon()); // set makespan to time-horizon (the worst possible case)
            s.append("\n");
            index++;
        }
        // write results to text file
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName);
            fw.write(s.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        print("Benchmark " + datasetName + " has been exported to " + fileName + " without solutions");
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#generateBenchmarkWithSolutions(cbrcps.algorithms.data.BenchmarkResults, java.lang.String)
     */
    @Override
    public void generateBenchmarkWithSolutions(BenchmarkResults benchmark, String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        // First generate the standard benchmark file containing the problem instances and their makespan
        String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
        StringBuilder s = new StringBuilder();
        s.append("=============================\n");
        s.append("Instance Makespan\n");
        s.append("------------------------------\n");
        int index = 1;
        for (BenchmarkSolution solution : benchmark.getBenchmarkSolutions()) {
            s.append(index);
            s.append("\t");
            s.append(solution.getOptimalSolutionMakespan());
            s.append("\n");
            index++;
        }
        // write results to text file
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName);
            fw.write(s.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        print("Benchmark " + datasetName + "has been exported to " + fileName + " with solutions");
        // Now, serialize the benchmark with all the solutions
        saveBenchmarkResults(benchmark, datasetName);
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#loadBenchmarkResults(java.lang.String)
     */
    @Override
    public BenchmarkResults loadBenchmarkResults(String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        // Now, serialize the benchmark with all the solution
        String fileName = path + datasetName + benchmarkType[datasetIndex] + ".ser";
        FileInputStream fis = null;
        ObjectInputStream in = null;
        BenchmarkResults benchmark = null;
        try {
            fis = new FileInputStream(fileName);
            in = new ObjectInputStream(fis);
            benchmark = (BenchmarkResults) in.readObject();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        print(datasetName + " solutions have been loaded from " + fileName);
        return benchmark;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#saveBenchmarkResults(cbrcps.algorithms.data.BenchmarkResults, java.lang.String)
     */
    @Override
    public void saveBenchmarkResults(BenchmarkResults benchmark, String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        // Now, serialize the benchmark with all the solution
        String fileName = path + datasetName + benchmarkType[datasetIndex] + ".ser";
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = new FileOutputStream(fileName);
            out = new ObjectOutputStream(fos);
            out.writeObject(benchmark);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        print(datasetName + " solutions have been saved to " + fileName);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    protected void print(String msg) {
        System.out.println(toString() + ": " + msg);
    }
}
