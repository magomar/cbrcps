/**
 *
 */
package cbrcps.wrappers;

/**
 * @author Mario Gomez
 *
 */
public final class DatasetDefinition {
	final private String path;
	final private String[] dataset;
	final private String[] benchmarkType;
	final private String benchmarkFormat;
	final private String problemFormat;
	final private String wrapperClass;


	/**
	 * @param path
	 * @param dataset
	 * @param benchmarkType
	 * @param benchmarkFormat
	 * @param problemFormat
	 * @param wrapperClass
	 */
	public DatasetDefinition(final String path, final String[] dataset, final String[] benchmarkType, final String benchmarkFormat, final String problemFormat, final String wrapperClass) {
		this.path = path;
		this.dataset = dataset;
		this.benchmarkType = benchmarkType;
		this.benchmarkFormat = benchmarkFormat;
		this.problemFormat = problemFormat;
		this.wrapperClass = wrapperClass;
	}

	public String getBenchmarkFormat() {
		return benchmarkFormat;
	}

	public String[] getBenchmarkType() {
		return benchmarkType;
	}

	public String[] getDataset() {
		return dataset;
	}

	public String getPath() {
		return path;
	}

	public String getProblemFormat() {
		return problemFormat;
	}

	public String getWrapperClass() {
		return wrapperClass;
	}
}