/**
 *
 */
package cbrcps.wrappers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.wrappers.DatasetDefinition;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class RCPWrapper extends AbstractDatasetWrapper {

	/**
	 * @param wrapper
	 */
	public RCPWrapper(DatasetDefinition wrapper) {
		super(wrapper);
	}

	/* (non-Javadoc)
	 * @see cbrcps.wrappers.DatasetWrapper#parseBenchmarkFile(java.lang.String)
	 */
	public BenchmarkResults parseBenchmarkFile(String datasetName) {
		int datasetIndex = getDatasetIndex(datasetName);
		String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
		print("Parsing dataset benchmark " + datasetName + " from " + fileName);
		File file = new File(fileName);

		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner lineScanner = new Scanner(fileScanner.nextLine());

		// Skips non useful lines
		while (!lineScanner.hasNext() && !lineScanner.next().equals("Instance")) lineScanner = new Scanner(fileScanner.nextLine());
		lineScanner = new Scanner(fileScanner.nextLine());

		//parse benchmark results for each individual instance
		ArrayList<BenchmarkSolution> solutions = new ArrayList<BenchmarkSolution>();
		while (fileScanner.hasNextLine()) {
			lineScanner = new Scanner(fileScanner.nextLine());
			if (lineScanner.hasNextInt()) {
				String problem = datasetName + lineScanner.nextInt();
				int upperBound = lineScanner.nextInt();
				int lowerBound = (benchmarkType[datasetIndex].equals("lb") ? lineScanner.nextInt() : upperBound);
				if (upperBound < 16384) {// if solution exists
					solutions.add(new BenchmarkSolution(problem, lowerBound, upperBound));
				}
			}
		}
		lineScanner.close();
		fileScanner.close();
		print("Dataset benchmark " + datasetName + " has been parsed");
		return new BenchmarkResults(datasetName, benchmarkType[datasetIndex],(BenchmarkSolution[]) solutions.toArray(new BenchmarkSolution[0]));
	}


	/* (non-Javadoc)
	 * @see cbrcps.wrappers.DatasetWrapper#parseProblem(java.lang.String, java.lang.String)
	 */
	public Project parseProblem(String datasetName, String problemName) {
		String fileName = path + datasetName + problemFormat + "/" + problemName + problemFormat;
		File file = new File(fileName);
		Project project = new Project();
		project.setName(problemName);
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner lineScanner;

		// Parse number of jobs (including supersource and sink)
		lineScanner = new Scanner(fileScanner.nextLine());
		project.setNumJobs(lineScanner.nextInt());

		// Parse number of resources
		project.setNumRenewableResources(lineScanner.nextInt());
		project.setNumNonRenewableResources(0);

		// Parse resource availabilities
		lineScanner = new Scanner(fileScanner.nextLine());
		lineScanner = new Scanner(fileScanner.nextLine());
		List<Resource> resources = project.getResource();

		for (int k = 0; k < project.getNumRenewableResources(); k++) {
			Resource resource = new Resource();
			resource.setId(k);
			resource.setRenewable(true);
			resource.setName("R" + (k + 1));
			resource.setCapacity(lineScanner.nextInt());
			resources.add(resource);
		}

		// Parse job durations, resource requests and successors
		List<Job> jobs = project.getJob();
		lineScanner = new Scanner(fileScanner.nextLine());
		for (int jobID = 0; jobID < project.getNumJobs(); jobID++) {
			Job job = new Job();
			lineScanner = new Scanner(fileScanner.nextLine());
			job.setId(jobID);// job number
			// parse resource requests
			job.setNumModes(1);
			Mode mode = new Mode();
			mode.setId(0);
			mode.setDuration(lineScanner.nextInt());
			List<Integer> resourceRequests = mode.getResourceRequest();
			for (int k = 0; k < project.getNumRenewableResources(); k++) {
				resourceRequests.add(Integer.valueOf(lineScanner.nextInt()));
			}

			List<Mode> modes = job.getMode();
			modes.add(mode);

			// parse successors
			job.setNumSuccessors(lineScanner.nextInt());
			List<Integer> successors = job.getSuccessor();
			for (int s = 0; s < job.getNumSuccessors(); s++) {
				successors.add(Integer.valueOf(lineScanner.nextInt() - 1));
			}
			jobs.add(job);
		}

		lineScanner.close();
		fileScanner.close();

		// rcp files do not include the time horizon in the file (as in psplib) so we must compute it
		int timeHorizon = 0;
		for (Job aJob : jobs) {
			timeHorizon += aJob.getMode().get(0).getDuration();
		}
		project.setTimeHorizon(timeHorizon);

		return project;
	}

	/* (non-Javadoc)
	 * @see cbrcps.wrappers.DatasetWrapper#exportProject(rcpsp.data.jaxb.Project, java.lang.String, java.lang.String)
	 */
    @Override
	public void exportProject(Project project, String datasetName, String problemName) {
		final String fileName = path + datasetName + problemFormat + "/" + problemName + problemFormat;
		StringBuilder s = new StringBuilder();
		Mode mode;
		// Append RCP description of the project to s and then write to file
		s.append(project.getNumJobs());
		s.append("\t");
		s.append(project.getNumRenewableResources());
		s.append("\n\n");
		for (Resource resource : project.getResource()) {
			s.append(resource.getCapacity());
			s.append("\t");
		}
		s.append("\n\n");
		for (Job job : project.getJob()) {
			mode = job.getMode().get(0);
			s.append(mode.getDuration());
			s.append("\t");
			for (Integer resourceRequest : mode.getResourceRequest()) {
				s.append(resourceRequest);
				s.append("\t");
			}
			s.append(job.getNumSuccessors());
			s.append("\t");
			for (Integer successor : job.getSuccessor()) {
				s.append(successor.intValue() + 1);
				s.append("\t");
			}
			s.append("\n");
		}

		// write results to text file
		FileWriter fw = null;
		try {
			fw = new FileWriter(fileName);
			fw.write(s.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
