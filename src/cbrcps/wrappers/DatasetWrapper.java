/**
 *
 */
package cbrcps.wrappers;


import cbrcps.algorithms.data.BenchmarkResults;
import java.util.List;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 *
 */
/**
 * @author Mario Gomez
 */
public interface DatasetWrapper {

	/**
	 * @param datasetName
	 * @param problemName
	 * @return
	 */
	public Project parseProblem(String datasetName, String problemName);
	/**
	 * @param datasetName
	 * @return
	 */
	public BenchmarkResults parseBenchmarkFile(String datasetName);
	/**
	 * @param project
	 * @param datasetName
	 * @param problemName
	 */
	public void exportProject(Project project, String datasetName, String problemName);
	/**
	 * @param newProjects
	 * @param datasetName
	 */
	public void generateBenchmark(List<Project>  newProjects, String datasetName);
	/**
	 * @param benchmark
	 * @param datasetName
	 */
	public void generateBenchmarkWithSolutions(BenchmarkResults benchmark, String datasetName);
	/**
	 * @return
	 */
	public String[] getDatasetName();
	/**
	 * @param benchmark
	 * @param datasetName
	 */
	public void saveBenchmarkResults(BenchmarkResults benchmark, String datasetName);
	/**
	 * @param datasetName
	 * @return
	 */
	public BenchmarkResults loadBenchmarkResults(String datasetName);
	/**
	 * @param benchmark
	 * @return
	 */
	public Project[] loadDatasetProjects(BenchmarkResults benchmark);
	/**
	 * @return
	 */
	public DatasetDefinition getDatasetDefinition();
	
}