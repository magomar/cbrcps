package cbrcps.executable.util;

import static cbrcps.util.Report.reportSolutions;
import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.algorithms.scheduler.branchandbound.SprecherDrexlHeuristic;
import cbrcps.algorithms.scheduler.branchandbound.SprecherDrexlHeuristicTimeLimited;
import cbrcps.algorithms.scheduler.multiheuristic.MultiHeuristicSSGS;
import cbrcps.experiment.TaskScheduler;
import rcpsp.data.jaxb.Project;
import cbrcps.util.Constant;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;

/**
 * @author Mario Gomez
 */
public final class GenerateBenchmarkSolutions {
	static final long timeLimit = 1800000; // ms, half an hour = 1800000
//	static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_b);
        static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.example_casebase);
//	static final Scheduler optimalScheduler = new SprecherDrexlHeuristicTimeLimited(timeLimit);
	static final Scheduler optimalScheduler = new SprecherDrexlHeuristic();
	static final Scheduler multiHeuristicScheduler = new MultiHeuristicSSGS(Constant.singleRule);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] datasetName = wrapper.getDatasetName();
		BenchmarkResults[] newBenchmark = new BenchmarkResults[datasetName.length];
		BenchmarkSolution[][] newBenchmarkSolution = new BenchmarkSolution[datasetName.length][];
		for (int d = 0; d < datasetName.length; d++) {
			BenchmarkResults benchmark  = wrapper.parseBenchmarkFile(datasetName[d]);
			Project[] project = wrapper.loadDatasetProjects(benchmark);
			Solution[] optimalSolution = TaskScheduler.solveMultipleProblemsConcurrently(optimalScheduler, project);
			Solution[] bestHeuristicSolution = TaskScheduler.solveMultipleProblemsConcurrently(multiHeuristicScheduler, project);
			newBenchmarkSolution[d] = new BenchmarkSolution[benchmark.getNumProblems()];
			int problemIndex = 0;
			for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
				newBenchmarkSolution[d][problemIndex] = new BenchmarkSolution(
						benchmarkSolution.getProblem(),
						optimalSolution[problemIndex].getMakespan());
				newBenchmarkSolution[d][problemIndex].setOptimalSolution(optimalSolution[problemIndex]);
				newBenchmarkSolution[d][problemIndex].setBestHeuristicSolution(bestHeuristicSolution[problemIndex]);
				problemIndex++;
			}
			newBenchmark[d] = new BenchmarkResults(benchmark.getDataset(), benchmark.getType(), newBenchmarkSolution[d]);
			wrapper.generateBenchmarkWithSolutions(newBenchmark[d],datasetName[d]);
		}
		System.out.println(" * * * FINISHED: GenerateBenchmarkResults * * *");

		// print results
		reportSolutions(newBenchmark, datasetName, "GenerateBenchmarkResults");
		System.exit(0);
	}

}
