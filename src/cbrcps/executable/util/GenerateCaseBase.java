/**
 *
 */
package cbrcps.executable.util;

import cbrcps.algorithms.data.BenchmarkResults;
import cbrcps.algorithms.data.BenchmarkSolution;
import cbrcps.algorithms.data.ProjectDescription;
import cbrcps.algorithms.data.Solution;
import rcpsp.data.jaxb.Project;
import cbrcps.util.Constant;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Mario Gomez
 *
 */
public final class GenerateCaseBase {
//	static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.casebase);

    static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.example_casebase);
    static final String dbGlobalPrefix = "create database cbrcps;\nuse cbrcps;\ndrop table cbrcps;\ncreate table cbrcps(caseId VARCHAR(16),dataset VARCHAR(16),numJobs INTEGER,numRenewableResources INTEGER,numNonRenewableResources INTEGER,networkComplexity FLOAT,renewableResourceFactor FLOAT,nonRenewableResourceFactor FLOAT,renewableResourceStrenght FLOAT,nonRenewableResourceStrenght FLOAT,activityList VARCHAR(256),priorityRule VARCHAR(128));\n";
    static final String dbInsertPrefix = "insert into cbrcps values(";
    static final String dbInsertSufix = ");\n";
    static final Locale locale = Locale.ENGLISH;
    static final NumberFormat nf = NumberFormat.getInstance(locale);

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("");
        System.out.println("=================== GENERATING CASE BASE ==========================");
        System.out.println("");
        String[] datasetName = wrapper.getDatasetName();
        StringBuilder s1 = new StringBuilder();
        s1.append(dbGlobalPrefix);
        int caseIndex = 0;
        for (int d = 0; d < datasetName.length; d++) {
            BenchmarkResults benchmark = wrapper.loadBenchmarkResults(datasetName[d]);
            Project[] project = wrapper.loadDatasetProjects(benchmark);
            StringBuilder s2 = new StringBuilder();
            int problemIndex = 0;
            for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolutions()) {
                Solution optimalSolution = benchmarkSolution.getOptimalSolution();
                Solution bestHeuristicSolution = benchmarkSolution.getBestHeuristicSolution();
                String heuristicAlgorithm = bestHeuristicSolution.getAlgorithm();
                String priorityRuleName = heuristicAlgorithm.substring(5, heuristicAlgorithm.length());
                ProjectDescription pd = new ProjectDescription(project[problemIndex]);
//				Compute global project parameters
                double nc = ProjectDescription.computeNetworkComplexity(pd);
                double rrf = ProjectDescription.computeRenewableResourceFactor(pd);
                double nrf = ProjectDescription.computeNonRenewableResourceFactor(pd);
                double rrs = ProjectDescription.computeRenewableResourceStrenght(pd);
                double nrs = ProjectDescription.computeNonRenewableResourceStrenght(pd);
                // Create sql code for storing the cases
                s2.append(dbInsertPrefix);
                s2.append("'" + benchmarkSolution.getProblem() + "',"); //Case ID = name of the problem
                s2.append("'" + datasetName[d] + "',");
                s2.append(project[problemIndex].getNumJobs() + ",");
                s2.append(project[problemIndex].getNumRenewableResources() + ",");
                s2.append(project[problemIndex].getNumNonRenewableResources() + ",");
                s2.append(nf.format(nc) + ",");
                s2.append(nf.format(rrf) + ",");
                s2.append(nf.format(nrf) + ",");
                s2.append(nf.format(rrs) + ",");
                s2.append(nf.format(nrs) + ",");
                s2.append("'" + optimalSolution.toStringJobs() + "',");
                s2.append("'" + priorityRuleName + "'");
                s2.append(dbInsertSufix);
                problemIndex++;
                caseIndex++;
            }

            // write results to text file
            String fileName = datasetName[d] + ".sql";
            String outputFile = Constant.datasetsPath + wrapper.getDatasetDefinition().getPath() + "/" + fileName;
            FileWriter fw = null;
            try {
                fw = new FileWriter(outputFile);
                fw.write(dbGlobalPrefix + s2.toString());
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Cases for dataset " + datasetName[d] + " exported to " + outputFile);
            s1.append(s2.toString());
        }
        // write results to global sql file
        String fileName = "cbrcps.sql";
        String outputFile = Constant.sqlPath + fileName;
        FileWriter fw = null;
        try {
            fw = new FileWriter(outputFile);
            fw.write(s1.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println(" * * * FINISHED: GENERATE CASES  * * *");
        System.out.println("Total number of cases generated: " + caseIndex);
        System.out.println("All cases exported to " + outputFile);
        System.exit(0);
    }
}
