package cbrcps.executable;

import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.experiment.CompareAlgorithms;
import cbrcps.experiment.Experiment;
import cbrcps.experiment.ExperimentFactory;
import cbrcps.util.Constant;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;

/**
 * @author Mario Gomez
 *
 */
public final class RunCompareAlgorithms {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareCBR(Constant.singleRule[0]);
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareHeuristics();
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareSampling();
		DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
		Experiment experiment =  new CompareAlgorithms("CompareAlgorithms", scheduler, datasetWrapper);
		experiment.runExperiment();
		experiment.endExperiment();
		System.exit(0);
	}

}
