/**
 * 
 */
package cbrcps.executable;

import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.algorithms.scheduler.branchandbound.SprecherDrexl;
import cbrcps.algorithms.scheduler.branchandbound.SprecherDrexlHeuristic;
import cbrcps.experiment.CompareSingleVsMultiThread;
import cbrcps.experiment.Experiment;
import cbrcps.experiment.ExperimentFactory;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;

/**
 * @author Mario Gomez
 *
 */
public final class RunSingleVsMultiThread {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Scheduler scheduler = ExperimentFactory.getAlgorithmsToCompareCBR()[4];
//		Scheduler scheduler = new SSGS();
		Scheduler scheduler = new SprecherDrexl();
//		Scheduler scheduler = new SprecherDrexlHeuristic();
		DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
		Experiment experiment =  new CompareSingleVsMultiThread("Compare Single vs Multi-Thread", scheduler, datasetWrapper);
		experiment.runExperiment();
		experiment.endExperiment();
		System.exit(0);
	}

}
