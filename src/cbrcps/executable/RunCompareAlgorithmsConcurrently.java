/**
 * 
 */
package cbrcps.executable;

import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.experiment.CompareAlgorithmsConcurrently;
import cbrcps.experiment.Experiment;
import cbrcps.experiment.ExperimentFactory;
import cbrcps.util.Constant;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;

/**
 * @author Mario Gomez
 *
 */
public final class RunCompareAlgorithmsConcurrently {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareCBR(Constant.singleRule[0]);
		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareHeuristics();
		DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_b);
		Experiment experiment =  new CompareAlgorithmsConcurrently("CompareAlgorithmsConcurrently", scheduler, datasetWrapper);
		experiment.runExperiment();
		experiment.endExperiment();
		System.exit(0);
	}

}
