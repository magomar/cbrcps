package cbrcps.executable.test;

import java.util.ArrayList;

import jcolibri.cbrcore.CBRQuery;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import cbrcps.algorithms.data.ProjectDescription;
import cbrcps.cbr.CBRCPS;
import cbrcps.cbr.CaseDescription;
import rcpsp.data.jaxb.Project;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;

public final class TestCBR {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		// Change this to select a different collection of datasets (eg. patterson, psplib_sm, psplib_mm)
		final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);

		// Change this to select a different problem
		final String dataset = "pat5";
		final String problem = "pat51";

		CBRCPS cbrcps = new CBRCPS(3);
		try {
			// Configure the application
			cbrcps.configure();
			// Execute the precycle
			cbrcps.preCycle();
		} catch (Exception e) {
			e.printStackTrace();
		}
//		Parse project
		Project project = wrapper.parseProblem(dataset, problem);
//		System.out.println(project.toString() + "\n");

//		Compute network parameters for the problem being solved
		ProjectDescription pd = new ProjectDescription(project);
		double nc = ProjectDescription.computeNetworkComplexity(pd);
		double rrf = ProjectDescription.computeRenewableResourceFactor(pd);
		double nrf = ProjectDescription.computeNonRenewableResourceFactor(pd);
		double rrs = ProjectDescription.computeRenewableResourceStrenght(pd);
		double nrs = ProjectDescription.computeNonRenewableResourceStrenght(pd);

//		Create a new query to retrieve similar problems solved in the past
		CBRQuery query = new CBRQuery();
		CaseDescription caseDescription = new CaseDescription();
		caseDescription.setNumJobs(project.getNumJobs());
		caseDescription.setNumRenewableResources(project.getNumRenewableResources());
		caseDescription.setNumNonRenewableResources(project.getNumNonRenewableResources());
		caseDescription.setNetworkComplexity(nc);
		caseDescription.setRenewableResourceFactor(rrf);
		caseDescription.setRenewableResourceStrenght(rrs);
		caseDescription.setNonRenewableResourceFactor(nrf);
		caseDescription.setNonRenewableResourceStrenght(nrs);
		query.setDescription(caseDescription);

		// Execute the cycle with the new query and get the retrieved cases
		try {
			cbrcps.cycle(query);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<RetrievalResult> retrievedCases = cbrcps.getRetrievedCases();
		if (retrievedCases.size() > 0) {
//			Print query and retrieved cases
			System.out.println("OK: Retrieved cases for query: " + query);
			for (RetrievalResult retrievalResult : retrievedCases) {
				System.out.println(retrievalResult);
			}
		} else {
			System.out.println("ERROR: No cases retrieved for query: " + query);
		}
		// Execute the postcycle
		try {
			cbrcps.postCycle();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.exit(0);
	}

}
