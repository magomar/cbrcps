/**
 *
 */
package cbrcps.executable.test;

import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.heuristics.jobselection.*;
import cbrcps.algorithms.matcher.BasicMatcher;
import cbrcps.algorithms.matcher.ProjectMatcher;
import cbrcps.algorithms.sampler.ModifiedRBRS;
import cbrcps.algorithms.sampler.Sampler;
import cbrcps.algorithms.scheduler.Scheduler;
import cbrcps.algorithms.scheduler.branchandboundv2.SprecherAndDrexl;
import cbrcps.graphs.ProjectToGraphConverter;
import cbrcps.util.ProjectUtil;
import rcpsp.data.jaxb.Project;
import cbrcps.wrappers.DatasetWrapper;
import cbrcps.wrappers.WrapperFactory;
import java.text.NumberFormat;
import org.miv.graphstream.graph.Graph;
import org.miv.graphstream.ui.GraphViewerRemote;

/**
 * @author Mario Gomez
 *
 */
public final class SolveOneProblem {

    static final NumberFormat nf = NumberFormat.getInstance();
    /**
     * CBR wrapper: accesses the problems referred by the cases in the case_base
     */
//    static final DatasetWrapper cbWrapper = WrapperFactory.obtainWrapper(WrapperFactory.casebase);
    static final DatasetWrapper cbWrapper = WrapperFactory.obtainWrapper(WrapperFactory.example_casebase);
    /**
     * target wrapper: accesses the new problems to be solved
     */
//	Change this to select a different collection of datasets (eg. patterson, psplib_sm, psplib_mm)
    static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.psplib_sm);
//	static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
    /**
     * Single dataset to select a problem from
     */
    static final String dataset = "j30";
    /**
     * Single problem to be soved (from the selected dataset)
     */
//    static final String problem = "j302_3";
    static final String problem = "j301_7";
    /**
     * Sampling parameter: number of iterations
     */
    static final int iterations = 1000;
    /**
     * Sampling parameter: epsilon
     */
    static final double epsilon = 1;
    /**
     * Sampling parameter: alpha
     */
    static final double alpha = 1;
    /**
     * Sampling parameter: lambda
     */
    static final double lambda = 10;
    /**
     * Heuristic Priority Rule
     */
    static final JobPriorityRule jobPriorityRule = new LST();
//    static final JobPriorityRule jobPriorityRule = new LFT();
    /**
     * Sampler
     */
    static final Sampler sampler = new ModifiedRBRS(alpha, lambda);
    /**
     * Project Matcher Algorithm, used to compare projects in the case base, which aims at finding simmilar projects.
     * The matcher is used by the retrieval algorithms when applying Case Based Reasoning to schedule projects.
     */
    static final ProjectMatcher matcher = new BasicMatcher();
    /**
     * k parameter for the K-NN case retrieval algorithm
     */
    static final int k = 1;

    /**
     * @param args
     */
    public static void main(String[] args) {
//        CBRCPS cbrcps = new CBRCPS(k);
//        try {
//            cbrcps.configure();
//            cbrcps.preCycle();
//        } catch (ExecutionException e) {
//        }

        // Parse project from file
        Project project = wrapper.parseProblem(dataset, problem);

        if (!ProjectUtil.checkCorrectness(project)) {
            System.out.println("The project is WRONG ! ");
        }

        System.out.println();
        System.out.println(project.toString() + "\n");

//		DirectedGraph<Node,Link> graph = ProjectToGraphConverter.convertToJGraphT(project);
//		System.out.println(graph.toString());

        Graph g = ProjectToGraphConverter.convertToGraphStream(project);
        GraphViewerRemote viewerRemote = g.display();
        viewerRemote.setQuality(4);	// 4 is the max quality (range is 0-4).
        g.addAttribute("ui.stylesheet", getStyleSheet(project));

        Solution newSolution;
        Scheduler scheduler;

//        scheduler = new SSGS(jobPriorityRule);
//        newSolution = scheduler.solve(project);
//        scheduler.close();
//        System.out.println(newSolution.toGanttDiagram() + "\n");
//        System.out.println(newSolution.toResourceAvailability() + "\n");

        scheduler = new SprecherAndDrexl();
        newSolution = scheduler.solve(project);
        scheduler.close();
		System.out.println(newSolution.toGanttDiagram() + "\n");
		System.out.println(newSolution.toResourceAvailability() + "\n");
                
//        scheduler = new SprecherDrexl();
//        newSolution = scheduler.solve(project);
//        scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//        scheduler = new SprecherDrexlHeuristic(jobPriorityRule);
//        newSolution = scheduler.solve(project);
//        scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//                scheduler = new SSGS_CBR(new CBPR_AR(jobPriorityRule), jobPriorityRule, cbrcps, cbWrapper, matcher);
//		newSolution = scheduler.solve(project);
//		scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//		Execute the postcycle, once the CBRCPS application is not needed anymore
//		try {
//			cbrcps.postCycle();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}

//		System.exit(0);
    }

    /**
     * @param project
     * @return
     */
    private static String getStyleSheet(Project project) {
        String s =
                "graph {"
                + "    color:yellow;"
                + "}"
                + "node {"
                + "	 width:20;"
                + "    text-color:black;"
                + "}"
                + "edge {"
                + "    width:1;"
                + "    color:black;"
                + "}"
                + "node#J0 {"
                + "    color:green;"
                + "}"
                + "node#J" + (project.getNumJobs() - 1) + " {"
                + "    color:red;"
                + "}";
        return s;
    }
}
