/**
 *
 */
package cbrcps.cbr;

import java.util.ArrayList;
import java.util.Collection;

import jcolibri.casebase.LinealCaseBase;
import jcolibri.cbraplications.StandardCBRApplication;
import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.connector.DataBaseConnector;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.NNScoringMethod;
import jcolibri.method.retrieve.NNretrieval.similarity.global.Average;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Equal;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Interval;
import jcolibri.method.retrieve.selection.SelectCases;
import cbrcps.cbr.database.HSQLDBServer;


/**
 * @author Mario Gomez
 */

public final class CBRCPS implements StandardCBRApplication {
	private final int k; // for the K-NN retrieval
	private ArrayList<RetrievalResult> retrievedCases;
	private Connector _connector;
	private CBRCaseBase _caseBase;


	/**
	 * @param k
	 */
	public CBRCPS(int k) {
		this.k = k;
	}


	/* (non-Javadoc)
	 * @see jcolibri.cbraplications.StandardCBRApplication#configure()
	 */
	@Override
	public void configure() throws ExecutionException {
		try {
			//	Emulate data base server
			HSQLDBServer.init();

			// Create a data base connector
			_connector = new DataBaseConnector();

			// Initialize the data base connector with the configuration file
			_connector.initFromXMLfile(jcolibri.util.FileIO.findFile("cbrcps/cbr/database/databaseconfig.xml"));

			// Create a linear case base for in-memory organization
			_caseBase = new LinealCaseBase();

		} catch (Exception e) {
			throw new ExecutionException(e);
		}

	}

	/* (non-Javadoc)
	 * @see jcolibri.cbraplications.StandardCBRApplication#preCycle()
	 */
	@Override
	public CBRCaseBase preCycle() throws ExecutionException {
		// Load cases from connector into the case base
		_caseBase.init(_connector);
//		Collection<CBRCase> cases = _caseBase.getCases();
//		for (CBRCase c : cases) {
//			System.out.println(c);
//		}
		return _caseBase;
	}


	/* (non-Javadoc)
	 * @see jcolibri.cbraplications.StandardCBRApplication#cycle(jcolibri.cbrcore.CBRQuery)
	 */
	@Override
	public void cycle(CBRQuery query) throws ExecutionException {
		// Configure the retrieval method (eg. NN method)
		NNConfig simConfig = new NNConfig();
		// Set the global similarity function
		simConfig.setDescriptionSimFunction(new Average());
		// Set the local similarity functions
		Attribute n = new Attribute("numJobs", CaseDescription.class);
		simConfig.addMapping(n, new Interval(10));
		simConfig.setWeight(n, 0.05);
		Attribute nRR = new Attribute("numRenewableResources", CaseDescription.class);
		simConfig.addMapping(nRR, new Equal());
		simConfig.setWeight(nRR, 0.3);
//		Attribute nNRR = new Attribute("numNonRenewableResources", CaseDescription.class);
//		simConfig.addMapping(nNRR, new Interval(3));
//		simConfig.setWeight(nNRR, 0.025);
		Attribute nc = new Attribute("networkComplexity", CaseDescription.class);
		simConfig.addMapping(nc, new Interval(2));
		simConfig.setWeight(nc, 0.25);
		Attribute rrf = new Attribute("renewableResourceFactor", CaseDescription.class);
		simConfig.addMapping(rrf, new Interval(1));
		simConfig.setWeight(rrf, 0.2);
		Attribute rrs = new Attribute("renewableResourceStrenght", CaseDescription.class);
		simConfig.addMapping(rrs, new Interval(1));
		simConfig.setWeight(rrs, 0.2);

		// Execute NN
		Collection<RetrievalResult> eval = NNScoringMethod.evaluateSimilarity(_caseBase.getCases(), query, simConfig);

		// Select k cases
		retrievedCases = new ArrayList<RetrievalResult>();
		retrievedCases.addAll(SelectCases.selectTopKRR(eval, k));

		// Print query and retrieved cases
		//		System.out.println("Retrieved cases for query: " + query);
		//		for (RetrievalResult retrievalResult : retrievedCases) {
		//			System.out.println(retrievalResult);
		//		}

		// Finally remove the retrieval info (similarity values) and get only the cases //obsolete
		//		Collection<CBRCase> retrievedCases = RemoveRetrievalEvaluation.removeRetrievalEvaluation(eval);

		// Get the best solution: that from the most similar case
		//		RetrievalResult bestResult = retrievedCases.get(0);
		//		CBRCase bestCase = bestResult.get_case();
		//		CaseSolution bestSolution= (CaseSolution) bestCase.getSolution();
		//		System.out.println(bestSolution);

	}

	/* (non-Javadoc)
	 * @see jcolibri.cbraplications.StandardCBRApplication#postCycle()
	 */
	@Override
	public void postCycle() throws ExecutionException {
		_connector.close();
		HSQLDBServer.shutDown();
	}


	/**
	 * @return the cases retrieved after executing one query 
	 */
	public ArrayList<RetrievalResult> getRetrievedCases() {
		return retrievedCases;
	}

	/**
	 * @return
	 */
	public int getK() {
		return k;
	}
}
