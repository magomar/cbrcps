package cbrcps.multithread;

import java.util.concurrent.Callable;

import cbrcps.algorithms.data.Solution;
import cbrcps.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 */
public final class SolveProblemExecutor implements Callable<Solution> {
	final Scheduler scheduler;
	final Project project;

	/**
	 * @param scheduler
	 * @param project
	 */
	public SolveProblemExecutor(Scheduler scheduler, Project project) {
		this.scheduler = scheduler;
		this.project = project;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Solution call() throws Exception {
		Solution solution = scheduler.solve(project);
		scheduler.close();
		return solution;
	}

}
