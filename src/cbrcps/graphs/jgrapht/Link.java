/**
 * 
 */
package cbrcps.graphs.jgrapht;

/**
 * @author Mario Gómez
 *
 */
public final class Link {
	private double weight;    // should be private for good practice
	private int id;
    
    public Link(int id, double weight) {
        this.id = id; 
        this.weight = weight;
    }
	
	public String toString() {
        return "E"+id;
    }
	
	public double getWeight() {
		return weight;
	}
}
